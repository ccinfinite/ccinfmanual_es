.. _concept_of_function:

*************************
El concepto de función
*************************

Esta parte del tutorial será una introducción algo más formal a las funciones. Proporcionaremos una definición completa y discutiremos los parámetros formales, los argumentos asignados por nombre y la sentencia de retorno. Puedes tomarlo como un desarrollo de lo que describimos en el capítulo anterior y consultar aquí cada vez que algo relacionado con las funciones te resulte poco claro.

.. index:: función

Como habrás leído antes, un trozo de código con nombre al que podemos hacer referencia varias veces se llamará función. En Python, construimos funciones con la palabra clave ``def``.

.. code-block:: Python

  def function_name (<argument_list>):
      BODY_FUNCTION
      <return object>

El rasgo característico de la función es el valor del resultado. En Python, este valor se declara mediante la sentencia ``return`` dentro del cuerpo de la función.
El uso de las funciones es intuitivo. Podemos suponer que cuando sea necesario repetir el código o ejecutar el mismo procedimiento, debemos transformarlo en una función. El programa se vuelve más legible y funcional. La idea de una función en programación es análoga a la de una función en sentido matemático. Sin embargo, a diferencia de las funciones matemáticas, las funciones de programación no necesitan tener un argumento (parámetro) para estar correctamente definidas. Por ejemplo:

.. activecode:: en_main_1001
   :caption: Función sin parámetro.

   def hey_function():
       return "¡Hola! ¿Cómo estás?"

   hey = hey_function()
   print(hey)

La declaración ``return``
=========================

.. index:: return

En los ejemplos anteriores, las funciones interactúan con nosotros a través de la instrucción ``return``. Esta instrucción pasa el objeto dado y hace que la función termine el cálculo. Si queremos pasar más de una variable, podemos utilizar una tupla, una lista o un diccionario. Para llamar a una función, escriba su nombre seguido de una tupla que contenga los argumentos de la función. El orden y el número de parámetros pasados a la función deben cumplir con su definición:

.. activecode:: en_main_1002
   :caption: Función que calcula el valor del cuadrado de la suma.

   def squared_sum(x, y):
       return (x + y) ** 2

   print(squared_sum(2, 1))

Tenga en cuenta que puede utilizar la sentencia ``return`` varias veces en el código de su función, pero la función dejará de ejecutarse y saldrá cuando se alcance cualquiera de ellas. Por ejemplo

.. activecode:: en_main_1003
   :caption: Una función que devuelve el mayor de los dos números dados.

   def maximum (x, y):
       """Una función que devuelve el mayor de dos números."""
       if x > y:
           return x
       else:
           return y

   print(maximum(10, -10))

.. index:: cadena de datos

La ejecución de la función se detiene tras la sentencia ``if`` o tras la sentencia ``else``. Es una buena práctica describir para qué sirve la función en forma de cadena ``"""..."""``. Una descripción, colocada inmediatamente después de la primera línea que define el nombre de la función, se denomina **cadena de datos**. En este curso, solemos discutir los programas en detalle. Para mantener las cosas simples, no siempre respetaremos esta práctica en los siguientes ejemplos. Sin embargo, debería hacerlo.

.. topic:: Ejercicio

    Escribe una función que compruebe si el número dado es par o impar.

Valores de los parámetros por defecto
========================================

.. index:: parámetros por defecto

Al definir una función, podemos asignar valores por defecto a los parámetros. Los valores por defecto se utilizarán cuando no se den otros argumentos para estos parámetros en la llamada a la función.

.. topic:: **Problema de física difícil**

    Has lanzado la pelota hacia arriba. Calcula a qué altura se elevará la pelota al cabo de \\(t\\) segundos si el valor de la velocidad inicial era \\(v_0\\) m∕s. Desprecia la fuerza de resistencia y todo lo demás, excepto la gravedad. Utiliza la fórmula

    .. math::

        h(t) = h_0 + v_0 t - \frac{1}{2} g t^2

En la fórmula anterior, \\(g\\) es la aceleración gravitatoria, que para la Tierra es aproximadamente \\(g = 9.80665 m/s^2\\). Permitimos cálculos de baja precisión y pretendemos que \\(g\\) es un valor constante (aunque no lo es...). Como la altura y la velocidad iniciales y el tiempo en que medimos la altura serán diferentes según el experimento, debemos dejarlas como variables sin los valores por defecto. Para la aceleración gravitatoria \\(g\\) podemos suponer que, por regla general, dicho lanzamiento tendrá lugar sobre el suelo (\\(h_0=0\\). En este caso, podemos dar los valores por defecto de la aceleración gravitatoria de la Tierra ``g`` y de la altura inicial \\(h_0\\) en la definición de la función ``height``.

.. activecode:: en_main_1004
   :caption: Función con valores de parámetros predefinidos.

   def height(t, v0, h0=0, g=9.81):
       """Función de altura:
          t - tiempo
          v0 - velocidad inicial
		  h0 - altura inicial, el valor por defecto es cero
          g - aceleración gravitacional, por defecto para la Tierra
        """
       return h0 + v0 * t - g * t ** 2 / 2

   print (height(0.3, 3))  # en la Tierra
   print (height(t=0.3, v0=3, g=1.622))  # en la Luna

.. topic:: Ejercicio

    Escribe una función que convierta la distancia dada en kilómetros a millas. Por defecto, convierte a millas náuticas.


Un número indefinido de parámetros de la función
=================================================

Python permite construir una función que no tenga definido el número de parámetros especificado. Para ello, precede el nombre de la lista de parámetros de la función con el símbolo ``*``. Podemos llamar a una función de este tipo con cualquier número de argumentos. La función presentada a continuación suma todos los valores pasados como argumentos.

.. activecode:: en_main_1005
   :caption: Una función con un número indefinido de parámetros.

   def addition(*arg):
       s = 0
       for a in arg:
           s += a
       return s

   print(addition(10))
   print(addition(10, 20, 30, 40))

¿Cómo funcionará este diseño junto con otros parámetros? Veamos.

.. activecode:: en_main_1006
   :caption: Un ejemplo de función en la que se desconoce el valor inicial de los parámetros.

   def addition(arg1, * arg):
       s = arg1
       for a in arg:
           s = s + a
       return s

   print(addition(100, 2))
   print(addition(100, 1, 2, 3, 4))

Funciones de anidamiento
=========================

.. index:: Funciones anidadas

Podemos incluir definiciones de funciones dentro del cuerpo de otras funciones. El programa siguiente calcula el valor del coseno del ángulo entre vectores en el plano:

.. activecode:: en_main_1007
   :caption: Contiene una función dentro de sí misma.

   def cosine(a, b):

       def length(x):
           return (x[0]**2 + x[1]**2)**0.5

       def product(x, y):
           return x[0]*y[0] + x[1]*y[1]

       m = length(a) * length(b)
       if m > 0:
           return product(a, b) / m

   print(cosine([1, 0], [0, 1]))


.. topic:: Ejercicio

    Escribe una función que para los valores dados ``a``, ``b`` y ``c`` encuentre soluciones al trinomio cuadrático.

    .. math::

        a x^2 + bx + c = 0

    El programa debe informar sobre el número de soluciones y devolverlas.
