.. _for_loop:

*************
Bucle ``for``
*************

.. index:: for

Esta sentencia ``for`` se utiliza para iterar a través de los elementos de una secuencia.

.. code-block :: python

    for element in sequence:
        BLOCK_OF_INSTRUCTIONS

Un bucle de este tipo tendrá exactamente ``len(sequence)`` revoluciones para que el bloque de instrucciones sea llamado exactamente tantas veces como elementos tenga la secuencia. Veamos un ejemplo.

.. activecode:: en_main_0801
    :caption: ejemplo de bucle for

    for el in 'Ala has a cat':
        print ('element: ' + el)

También podemos declarar una variable y luego iterar sobre ella:

.. activecode:: en_main_0802
    :caption: otro ejemplo de secuencia

    seq = (1, 25, 100, 10000)
    for el in seq:
        print('otro ejemplo de raíz secuencial cuadrada de', el, "=", el ** 0.5)

Por supuesto, en lugar de la variable ``seq`` podemos poner cualquier variable con elementos: lista, tupla, cadena o diccionario. En el caso de esta última, iteraremos sobre sus claves.

.. activecode:: en_main_0803
    :caption: diccionarios y bucle for

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    for name in birthday:
        print(name, 'has birthday', birthday[name])


.. index:: range

Función ``range``
~~~~~~~~~~~~~~~~~~

``range`` es una práctica función que crea objetos iterables que podemos utilizar directamente en un bucle ``for``. Crea una secuencia de enteros.

.. code-block:: python

    range(start, stop, step)

Es bastante similar al corte de secuencias.
Esta función devolverá una secuencia de números,
empezando por el valor ``start`` y terminando con
``stop - 1`` exactamente cada ``step``.
Veamos un ejemplo.

.. activecode:: en_main_0804

    seq = range(1, 11, 2)
    for el in seq:
        print(el)

El código anterior mostrará los números ``1, 3, 5, 7, 9``. Los números ``start`` y ``step`` son opcionales. Si damos un solo número ``A`` a la función ``range(A)`` obtendremos todos los enteros desde ``0`` hasta el número ``A - 1``. Si damos dos números, el primero se interpretará como ``start`` y el segundo como ``stop``. Sólo podemos definir el paso especificando los tres números. Por supuesto, son posibles las cantidades negativas, incluyendo un valor negativo para un ``step``. Esto nos da la posibilidad de crear una lista de números decrecientes. Debemos recordar que el valor inicial debe ser mayor que el valor final.

.. activecode:: en_main_0805
    :caption: bucle for iterando a través de la secuencia generada a partir de la función range

    for number in range(200, 100, -25):
        print(number)

.. topic:: Ejercicio

    Volvemos a realizar el ejercicio de la lección anterior. Utiliza la función ``range``, las sentencias ``if`` y ``for`` para encontrar la suma de todos los números divisibles por 5 y 7 en el rango 2998 a 4444. Deberías obtener el valor 152110.

.. index:: iterator

La función 'range' crea un tipo especial de secuencia llamada **iterador**. No entraremos en detalles ahora, pero como este nombre se encuentra a menudo en los tutoriales o libros sobre el lenguaje Python, conviene recordar que si utilizamos un iterador de este tipo en un bucle, el resultado de su operación será el mismo que el de las secuencias típicas (como las listas). Usando iteradores, ahorramos memoria porque no crean una lista completa en la memoria del ordenador, sino que en cada iteración calculan el siguiente valor, recordando sólo el valor actual, el paso y el valor final.

La función ``enumerate``
~~~~~~~~~~~~~~~~~~~~~~~~~~

A veces, teniendo una secuencia, queremos iterar sobre aquellos elementos que están en algún lugar que nos interesa. Por ejemplo, en la segunda mitad de una lista o posicionados en los índices impares. Podemos utilizar con éxito los índices de los elementos, pero el bucle ``for`` itera sobre los elementos, no sobre los índices. Para no utilizar las soluciones estándar para esto, es decir, introducir una variable adicional que desempeñará el papel de un índice, como a continuación

.. code-block:: python

  idx = 0
  for element in sequence:
      if idx > len(sequence)/2:
          # hacer algo con los elementos
          # de la segunda mitad de la secuencia

podemos utilizar la función ``enumerar(secuencia)`` que devuelve una tupla de pares ``(índice, elemento)``. Para utilizar ambas cantidades simultáneamente, podemos escribir

.. activecode:: en_main_0806
    :caption: ejemplo enumerar

    sequence = sorted([123, 12, 12, 1, 4, 1, 124, 13, 441])
    sum = 0
    for index, element in enumerate(sequence):
        if index > len (sequence)/2:
            sum += element
    print('Suma de elementos mayor que la mediana:', sum)
