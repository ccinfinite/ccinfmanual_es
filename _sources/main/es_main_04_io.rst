.. _io:

******************************
Entrada y salida
******************************

Las operaciones de entrada y salida se encargan de leer y escribir datos, respectivamente.
Las operaciones básicas que nos interesarán están relacionadas con la simple comunicación con el usuario. En definitiva, nos referimos a la visualización de información en la pantalla y a la lectura de los caracteres introducidos desde el teclado. Para ello se utilizan las funciones ``print`` y ``input``, respectivamente.

.. index:: print, formato de impresión simple

La función ``print``
~~~~~~~~~~~~~~~~~~~~~~

Para escribir algo en la pantalla, tenemos que utilizar la función ``print``.  Justo después de su nombre, debemos poner entre paréntesis el objeto que queremos ver. Puede ser algún valor dado explícitamente o una variable.

.. code-block:: python

    >>> print (13)
    13
    >>> z = 13
    >>> print (z)
    13

En este formato sencillo, sólo mostramos el contenido de las variables. También podemos mostrar una serie de variables/valores. Basta con separarlas con la coma, una a una.

.. activecode:: en_main_0401
    :caption: imprimir - formato simple

    name = 'Ala'
    age = 13
    print(name, "is", age, "years old")

.. topic:: Ejercicio

    Utilizando el ejemplo anterior, intenta añadir algunas variables más y mostrar que ``'Ala is 13 years old, lives in the city of Katowice, and her favorite sport is basketball.'`` con la función ``print``.

Argumentos opcionales para la función ``print``
-----------------------------------------------

.. index:: imprimir argumento sep

El carácter por defecto que separa los elementos de la lista de valores impresos por la función ``print`` es un espacio. Puedes verlo en el ejemplo anterior. Sin embargo, podemos cambiar este signo libremente. Para ello se utiliza el argumento 'sep'. Para cambiar el espacio por defecto por un signo '+', escriba

.. code-block:: python

    >>> print('Ala', 'is', '13', 'years', 'old', sep='+')
    'Ala+is+13+years+old.'

Para sustituir ``+`` por ``---`` basta con reemplazar el separador dado por el nuevo que se desea.

.. showeval:: showEval_print_sep
   :trace_mode: false

   print('Ala', 'is', '13', 'years old.', sep='+')
   ~~~~
   print('Ala', 'is', '13', 'years old.', sep='{{+}}{{---}}')


Es sencillo, ¿no? Pruebe usted mismo. Abajo tienes *CódigoActivo*. Sustituye ``+`` por otro separador, como: ``'@('_')@'``, verás cómo funciona.

.. activecode:: en_main_0402
   :caption: print - sep

   print('Ala', 'is', '13', 'years old.', sep='+')


.. index:: imprimir argumento fin

Al igual que con el separador, podemos determinar de forma independiente cómo terminará ``print`` la visualización de la lista de argumentos. Para ello, utilizamos el argumento ``end``. Por defecto, ``print`` rompe la línea y pasa a una nueva, lo que significa que ``end`` se establece en el comando *break line*, que escribimos ``end='\n'``. Los caracteres ``\n`` son caracteres especiales que significan un salto de línea. Hay más de ellos. Si te interesa, consulta la parte de este manual que trata de la manipulación de cadenas.

Cuando damos cuatro variables en cuatro llamadas a la función ``print``, las veremos en cuatro líneas separadas.

.. code-block:: python

   print('Ala')
   print('is')
   print('13 ')
   print('years old.')
   Ala
   is
   13
   years old.

Por otro lado, si establecemos el argumento ``end=''`` (o cualquier otro, similar a ``sep``), veremos que print no irá a una nueva línea después de mostrar la cadena, sino que en lugar de romperla, pondrá el carácter dado allí. Observa que el último ``print`` en el código de abajo debería romper la línea.

.. activecode:: pl_main_0403
    :caption: print - end

    print('Ala', end=' ')
    print('is', end='... ')
    print('13', end="!!! ")
    print('years old.', end='\n')

.. index:: imprimir argumento a ras de suelo, imprimir argumento de archivo

Por último, mencionaremos otros dos argumentos opcionales: ``file`` y ``flush``. Se utilizan cuando se escribe el contenido de las variables en un fichero. A la variable ``file`` se le pasa el handle del fichero que queremos añadir. El segundo argumento, ``flush`` es una variable booleana. Permite forzar la impresión del contenido de forma inmediata (cuando lo establecemos como ``True``) o almacenarlo en caché (``False``). No nos preocupemos de ello por ahora, porque no lo necesitaremos por ahora.
Ya hablaremos en otra ocasión de los ficheros y los handles.


La declaración ``input``
~~~~~~~~~~~~~~~~~~~~~~~~~

A veces queremos introducir un poco de interacción en nuestros programas, preguntar al usuario algo y esperar una respuesta (sí/no). Para pasar algún valor a nuestro programa de forma dinámica, ya sea un número o algún texto, utilizamos la función ``input(message)``. Este ``message`` es una cadena que el usuario verá antes de introducir un valor. Veamos cómo funciona.

.. activecode:: en_main_0404
    :caption: input

    name = input("¿Cómo te llamas? ")
    age = input('How old are you? ')
    print(name, 'tiene', age, 'años.')


Los datos introducidos por el usuario no se interpretan, y el tipo de valor almacenado en la variable es siempre una cadena (``str``).

A veces, sin embargo, los valores dados por el usuario son numéricos, y nos gustaría capturarlos en las variables como números para calcular algo a partir de ellos. Para ello, basta con convertir explícitamente lo que toma la función ``input`` al tipo que nos interesa. Para convertir los datos recuperados a un número entero, tenemos que utilizar la función int. Para los números de punto flotante, utilizaremos la función float.

.. activecode:: en_main_0405
    :caption: input, int y float

    cats = int(input('¿Cuántos gatos tienes?: '))
    print('Estáis en casa', 4 * cats, 'piernas peludas!')

    height = float(input('¿Cuánto mide (en metros)? '))
    print('Usted tiene {} centímetros!'.format(height * 100))


.. topic:: Ejercicio

    Utilizando el ejercicio anterior y el ejemplo de arriba, escriba un programa corto que pida al usuario alguna información adicional. Además del nombre y la edad, pregúntale por su deporte favorito y la ciudad en la que vive. Finalmente, utiliza ``print`` para mostrar esta información de la forma más clara posible. Puedes utilizar los ejemplos anteriores y el *ActiveCode* de arriba para programar tu solución.
