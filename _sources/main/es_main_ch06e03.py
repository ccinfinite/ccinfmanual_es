# Intenta reescribir el programa anterior para pedir al usuario un nombre
# del mes y en base a él devuelva información sobre el número de días.

days_31 = [1, 3, 5, 7, 8, 10, 12]
days_30 = [4, 6, 9, 11]
month = int(input('Introduzca el número del mes (de 1 a 12): '))
if month == 2:
   print('El mes nr {} tiene 28 o 29 días'.format(month))
elif month in days_31:
   print('El mes {} tiene 31 días.'.format(month))
elif month in days_30:
   print('El mes {} tiene 30 días.'.format(month))
else:
   print('Un año tiene 12 meses.')
