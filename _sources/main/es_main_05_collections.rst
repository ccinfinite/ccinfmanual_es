.. _complex_data_types:

**************************************************
Algunos de los tipos de complejos más populares
**************************************************

A veces es conveniente agrupar objetos, por ejemplo, creando una lista de nombres de alumnos en clase, una lista de canciones favoritas de Pearl Jam o creando un catálogo de libros. También podemos guardar las calificaciones de elementos específicos en colecciones similares. Crear variables separadas para tus cinco canciones favoritas de Pearl Jam es factible, pero no muy eficiente.

.. code-block:: python

   pj_no1 = 'Black'
   pj_no2 = 'Crazy Mary'
   pj_no3 = 'Rearviewmirror'
   pj_no4 = 'Better Man'
   pj_no5 = 'Sirens'

Sería más prudente hacer una lista de estas canciones y poner tu canción favorita *en la lista actual* en la primera posición, tu segunda canción favorita en la segunda posición, etc.

.. index:: lista, lista, objeto modificable

Listas
~~~~~~~

Una **lista** (el tipo ``list`` de Python) puede ser útil.
La lista es el tipo más general de secuencia. Al igual que :ref:`strings <strings>` los objetos de este tipo son secuencias ordenadas. La principal ventaja de las listas es la simplicidad de procesamiento de sus elementos. Las operaciones son similares a las de las cadenas, ya que podemos trocearlas, hacer referencia a los índices o comprobar su longitud. Podemos crear una variable de tipo ``list`` de varias maneras. La más básica será utilizar corchetes, que es una **interfaz** dedicada a las listas.

.. code-block:: python

    L1 = [10, 20, 30]
    L2 = []

El primer objeto llamado ``L1`` será una lista con tres elementos numéricos ``10, 20, 30`` y el segundo ``L2`` será una lista vacía (una lista sin elementos). Por lo tanto, la primera longitud es tres, y la segunda es 0 - puedes comprobarlo con la misma función que utilizaste para comprobar el tamaño de la cadena ``len(list)``. Volviendo al ejemplo de la lista de canciones, quedaría así

.. activecode:: en_main_0501
    :caption: Los cinco mejores de Pearl Jam

    pj = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print(pj)


Como puede ver, la primera canción de la lista es la que se asignó previamente a la variable que denota el éxito número uno. Para referirse a este nombre, utilizaremos la misma referencia por índice que para las cadenas.

.. code-block:: python

    pj[0]

Devolverá 'Black'.

.. index:: concatenación de listas

Las listas se pueden concatenar mediante el operador ``+``.
Podemos crear una nueva lista combinando estas dos listas con el operador de suma ``L1 + L2``. La nueva lista estará formada por todos los elementos de ambas listas. Los elementos de ``L1`` estarán al principio, y los de ``L2`` se encontrarán al final.

.. activecode:: en_main_0502
    :caption: Los diez mejores de Pearl Jam

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    pj2 = ['Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once']
    pj = pj1 + pj2
    print(pj)


Podemos modificar las listas. La modificación más sencilla es cambiar un elemento concreto utilizando la referencia ``list[index]`` y usar el operador de asignación. Podemos, por ejemplo, decidir que a partir de hoy, la quinta mejor canción de Pearl Jam para nosotros no será *Sirens*, sino *Hail, Hail*. En este caso, basta con referirse al quinto lugar de la lista (recordando que empezamos a numerar desde cero) y asignar una nueva canción.

.. showeval:: showEval_list_mod
   :trace_mode: true

   pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
   ~~~~
   {{'Sirens' --> 'Hail, Hail'}}{{pj1[4] = 'Hail, Hail'}}
   {{pj1[4] = 'Hail, Hail'}}{{}}
   pj1 --> ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', {{'Sirens'}}{{'Hail, Hail'}}]

Pruébalo tú mismo. También puedes cambiar *Hail, Hail* por un título completamente diferente.

.. activecode:: pl_main_0503
    :caption: modificación por referencia a un índice

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Oryginalna lista', pj1, sep='\n')
    pj1[4] = 'Hail, Hail'
    print('Zmodyfikowana lista', pj1, sep='\n')


Al igual que con las cadenas, podemos utilizar :ref:`cut sequence <sequence_slicing>`, mediante referencia conocida.

.. code-block:: python

    list[start_index:end_index:step]


Modificaciones y otras operaciones específicas de las listas
-------------------------------------------------------------

.. index:: OOP, objeto, método, atributo

Python es un lenguaje orientado a objetos. Te contaremos más sobre él al final de este manual. Por ahora, es suficiente saber que un objeto contiene tanto datos como métodos para manipular estos datos. Puedes entender los datos como unos valores que asignas a las variables. Por ejemplo, ``L = [1, 2, 3]`` sería una lista llamada ``L`` con tres elementos ``1, 2, 3``.

Para operar sobre una lista de este tipo (así como sobre otros objetos) utilizando métodos incrustados en ellos, utilizamos la llamada **referencia por un punto**. La sintaxis de dicha referencia es.

.. code-block:: python

    # referencia del método
    object.method(arguments)
    # referencia a un atributo (campo)
    object.attribute

.. index:: append

Las listas tienen varios métodos de este tipo, y los discutiremos al final de esta sección. Por ahora, nos centraremos en uno de los más utilizados: el método ``append`` para ampliar la lista con otro elemento. Si ya tenemos la lista mencionada anteriormente de las cinco canciones favoritas de Pearl Jam ``pj1``, pero queremos añadirle dos piezas más, entonces podemos escribir

.. activecode:: en_main_0504
    :caption: modificación por una referencia de índice

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Lista original', pj1, sep='\n')
    pj1.append('Hail, Hail')
    print('Lista extendida', pj1, sep='\n')
    pj1.append('You Are')
    print('List of 7 elements', pj1, sep='\n')

Esperamos que lo entiendas porque ahora es el momento de hacer un ejercicio...

.. topic:: Ejercicio

    Utilizando el método ``append``, amplía la lista de reproducción original ``pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']`` con otras 5 canciones: 'Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once' (o cualquier otra que elijas). Puedes utilizar el *ActiveCode* anterior o programar la respuesta en el cuaderno Jupyter.


Hay muchos métodos para manipular listas.

* ``L.append(obj)``: añade un elemento al final de la lista ``L``
* ``L.extend(new_list)``: Extiende la lista incluyendo todos los elementos de la lista especificada ``new_list``.
* ``L.insert(idx, obj)``: inserta el elemento ``obj`` en la posición dada la lista ``idx``
* ``L.remove(obj)``: elimina de la lista el primer elemento ``obj`` encontrado; si no existe tal elemento en la lista, se informa de un error
* ``L.pop(idx)``: elimina un elemento de la lista a partir del elemento ``idx`` dado y lo devuelve como resultado; si no se da ningún índice, ``L.pop()`` elimina y devuelve el último elemento de la lista
* ``L.index(obj)``: Devuelve el índice del primer elemento de la lista
* ``L.count(obj)``: Devuelve el número de apariciones de ``obj`` en la lista
* ``L.sort()``: ordena los elementos de la propia lista; es una operación que modifica irreversiblemente la lista ``L``
* ``L.reverse()``: Invierte el orden de los elementos de la propia lista

Vamos a probar algunas de estas operaciones: Haremos una lista L y añadiremos los cuadrados de los números del 1 al 10. Luego quitaremos y añadiremos algunos números por varios métodos.

.. activecode:: en_main_0505
    :caption: algunas operaciones con listas

    L = [10, 20, 30]
    print('inicio: L =', L)

    L.insert(0, 1001)
    L.append(100)
    print('append + insert: L =', L)

    L.pop(2)
    del L[2]
    print('pop + del: L =', L)

    L.reverse()
    print('reverso: L =', L)


.. index:: tupla

Tuplas
~~~~~~

Las tuplas son listas que no podemos modificar. Las creamos
con paréntesis ``()``.

.. code-block:: python

    three_elements_tuple = (1, 2, 3)
    single_element_tuple = ('Ala',)
    empty_tuple = ()

En el ejemplo anterior, la ``three_elements_tuple`` es una tupla con tres elementos (``len(three-element tuple)`` devolverá el número 3),
Una ``single_element_tuple`` es una tupla con un elemento
y ``empty_tuple`` es una tupla vacía.
Tenga en cuenta la coma obligatoria al crear tuplas de un solo elemento ``('Ala',)``. Las tuplas se pueden concatenar utilizando el operador ``+``.

.. activecode:: en_main_0506
    :caption: concatenación de tuplas

    k1 = (1, 2, 3)
    k2 = ('Ala', 'has', 'a cat')
    k3 = k1 + k2
    print(k3)

Las tuplas como objetos inmutables tienen los métodos limitados a ``count`` y ``index``, que funcionan precisamente como los métodos de lista apropiados.

.. activecode:: en_main_0507
    :caption: two tuple methods

    k1 = (1, 1, 3, 2, 3, 3)
    print('k1.count(3):', k1.count(3))
    print('k1.index(3):', k1.index(3))

Un elemento de tupla no puede ser eliminado con el comando ``del``, ya que no podemos modificarlos.


.. index:: dict

Diccionarios
~~~~~~~~~~~~

Aparte de los dos **tipos secuenciales** anteriores, otro tipo complejo útil y utilizado con frecuencia son los **diccionarios**. Lo que los distingue de las listas, tuplas y cadenas es su falta de ordenación y el hecho de que cada elemento de un diccionario consta de dos partes: una clave y un valor. La clave es una cantidad que identifica el valor en el diccionario. Los diccionarios se construyen con llaves.

.. code-block:: python

  dictionary = {key1: value1, key2: value2, key3: value3}


Si, por ejemplo, queremos crear una base de datos con información sobre los cumpleaños de nuestros amigos, podemos utilizar un diccionario. Utilizaremos las claves para marcar los nombres de los amigos, y el valor será la fecha de nacimiento.


.. code-block:: python

     birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}


Cuando queremos recordar la fecha de nacimiento de Amanda, tenemos que referirnos al elemento cuya ``clave = 'Amanda'``, utilizando corchetes, de forma similar a las listas, tuplas y cadenas.


.. activecode:: en_main_0508
    :caption: Birthday dictionary

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday['Amanda'])


De este modo, podemos saber en qué día cumplen años nuestros amigos si accidentalmente lo olvidamos.

.. topic:: Ejercicio

     Pruebe el *Código activo* anterior sustituyendo el nombre por algún otro nombre de la lista. También puede añadir sus propios pares ``name: birthdate``.

De forma relativamente sencilla, podemos modificar elementos ya existentes del diccionario. Basta con referirse a un elemento del diccionario con una clave y asignarle un nuevo valor.

.. activecode:: en_main_0509
    :caption: Modificación de un elemento del diccionario

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    birthday['Amanda'] = 'August 30'
    print(birthday)

De forma similar a los tipos complejos anteriormente discutidos, los diccionarios tienen muchos métodos incorporados que le permiten manipularlos. Todos ellos están disponibles utilizando la referencia punto. Cubriremos algunos de ellos en un momento. Ahora nos centraremos en la posibilidad de ampliar el diccionario con nuevos elementos. La opción más sencilla es introducir una nueva clave y un nuevo valor como si quisiéramos modificar un par existente.

.. activecode:: en_main_0510
    :caption: Añadir un elemento del diccionario

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    birthday['Mike'] = 'November 20'
    print(birthday)

.. topic:: Ejercicio

    En el *CódigoActivo* anterior, añade el nombre y el día de nacimiento de alguien que conozcas. También podría ser alguien tan famoso como Albert Einstein.

.. index:: del

Para eliminar fácilmente un par ``clave: valor`` del diccionario, podemos utilizar el comando ``del`` para eliminar las variables del espacio de nombres.

.. activecode:: en_main_0511
    :caption: Delete a dictionary item

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    del birthday['Carol']
    print(birthday)

Además de estas operaciones básicas mediante la interfaz de corchetes, los diccionarios pueden modificarse mediante métodos incorporados en ellos. A continuación encontrará una lista de métodos con una breve descripción. Asumimos que ``d`` es un diccionario.


* ``d.clear()`` Elimina todos los elementos del diccionario ``d``
* ``d.copy()`` Devuelve una copia del diccionario ``d``
* ``d.fromkeys(klucze, default_value)`` Devuelve el diccionario ``d`` con las claves especificadas ``keys`` y el valor establecido en la variable opcional ``default_value``
* ``d.get(klucz, default_value)`` Devuelve el valor de la clave dada ``key``, si no hay clave devuelve el valor de la variable ``default_value``
* ``d.items()`` Devuelve una lista que contiene una tupla por cada par ``clave: valor``
* ``d.keys()`` Devuelve una lista que contiene las claves de un diccionario
* ``d.pop(klucz)`` Elimina un elemento con la clave especificada ``key``
* ``d.popitem()`` Elimina el último par clave-valor insertado
* ``d.setdefault(key, value)`` Devuelve el valor de la ``clave`` especificada. Si la ``clave`` no existe, crea un par ``clave: valor`` en el diccionario ``d``.
* ``d.update(d)`` Actualiza el diccionario ``s`` con elementos del diccionario ``d`` sobrescribiendo los pares existentes
* ``d.values()`` Devuelve una lista de todos los valores del diccionario

Las operaciones aprendidas permiten una manipulación relativamente libre de los diccionarios. Por ejemplo, podemos utilizar dos formas de modificar el diccionario de fecha de nacimiento.

.. activecode:: en_main_0512
    :caption: Modificación de un elemento del diccionario

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    # 1
    birthday['Carol'] = 'August 3'
    print(birthday)
    # 2
    birthday.update({'Carol': 'August 4'})
    print(birthday)
