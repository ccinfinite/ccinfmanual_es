.. _subroutines:

************
Subrutinas
************

De vez en cuando, para tomar una decisión, decidimos que el destino ciego decida por nosotros. En esos momentos, solemos `tirar una moneda <https://en.wikipedia.org/wiki/Coin_flipping>`_ y según salga cara o cruz, elegimos una determinada solución. Por ejemplo, los partidos de fútbol o baloncesto comienzan con el lanzamiento de una moneda. Este método garantiza que la selección de un bando ganador es completamente aleatoria y no depende de ningún lanzamiento anterior. A veces resolvemos una disputa con ese lanzamiento y esperamos que la moneda nos dé la razón. Puede que ganemos una vez o tres, pero cada lanzamiento es independiente. Siempre podemos calcular la probabilidad de que (a) siempre o (b) la mitad de las veces ganemos. Para estos cálculos, utilizamos la distribución binomial, que nos dirá la probabilidad de :math:`k` aciertos en \\(n\\) ensayos.

.. math::

    \frac{n!}{k!(n-k)!} p^k (1-p)^{n-k}

¡El valor :math:`p=1/2` significa que tenemos la misma probabilidad de que salga cara o cruz, :math:`p^k` denota la exponenciación, y :math:`n! = 1 \cdot 2 \cdot \dots n` representa el factorial de \\(n\\). La exponenciación tiene su operador en Python, y podemos escribir esta operación como ``p ** k``. El factorial aparece como una función en la biblioteca ``math``, pero es fácil de calcular usando la fórmula dada y algún bucle. El siguiente código calcula el factorial de \\(10! = 3628800\\).

.. activecode:: pl_main_0901
    :caption: factorial

    n = 10
    factorial = 1
    for number in range(1, n + 1):
        factorial = factorial * number
    print(factorial)


Ya sabemos calcular el factorial y la potencia. Por lo tanto, podemos calcular la probabilidad de ganar la apuesta cinco veces de cada 10 lanzamientos. Volvemos a la fórmula, pero la sustituiremos por números concretos.

.. math::

    \frac{10!}{5!(10 - 5)!} 0.5^5 (1-0.5)^{10-5}

Tenemos que calcular el factorial de 10 y 5 y llevar :math:`1/2` a la quinta potencia.

.. activecode:: en_main_0902
    :caption: 5 victorias de lanzamiento de moneda en 10 intentos
    :enabledownload:


    n = 10
    factorial10 = 1
    for number in range(1, n + 1):
        factorial10 = factorial10 * number

    n = 5
    factorial5 = 1
    for number in range(1, n + 1):
        factorial5 = factorial5 * number

    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1 - 0.5) ** (10 - 5))
    print ('La posibilidad de ganar cinco veces en 10 intentos es {: .1f}%'. format(p5 * 100))

¿Esperabas un 50%? Es fácil caer en esta trampa... Afortunadamente, esto no es una clase de combinatoria sino un curso de programación. Volvamos al código anterior por un momento. Las líneas que calculan el factorial de 10 (1-4) y 5 (6-9) son copias del mismo código. La única diferencia es el valor de la variable ``n`` y los nombres de las variables resultado (``factorial5`` y ``factorial10``). Probablemente la forma más fácil de calcular la probabilidad de 5 victorias sería si tuviéramos el operador ``!``, que calcula el factorial de un número natural dado. No vamos a cambiar la biblioteca estándar de Python, pero en cierto modo, podemos programar cualquier nueva funcionalidad construyendo una **subrutina**. Podemos dividir las subrutinas en procedimientos y **funciones**. En Python, sólo nos ocupamos de estas últimas.

.. index:: función

Función
===============

Por función, entendemos un trozo de código con nombre que podemos referenciar repetidamente. Para construir una función, utilizamos la siguiente sintaxis.

.. code-block:: Python

  def function_name(<argument_list>):
      BODY_FUNCTION
      <return object>

Los objetos que ves entre paréntesis angulares (``argument_list`` y ``return object``) no son obligatorios y pueden omitirse al construir una función. Sin embargo, normalmente la función transformará la lista de argumentos (**datos de entrada**) en ese ``objeto``, que luego devolverá (**datos de salida**) con la sentencia **return**. Para este tutorial, acordemos que construiremos funciones que contengan la palabra clave **return**.

Intentemos reescribir el programa que calcula la probabilidad de 5 victorias utilizando la función factorial. El dato de entrada es el número ``n``, y el dato de salida es el factorial de este número.

.. activecode:: en_main_0903
    :caption: 5 lanzamientos de moneda ganan en 10 intentos, esta vez con la función
    :enabledownload:

    def factorial(n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    factorial10 = factorial(10)
    factorial5 = factorial(5)
    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1-0.5) ** (10-5))
    print ('La posibilidad de ganar cinco veces en 10 lanzamientos es {: .1f}%'. format(p5 * 100))

No es un operador, pero es igual de sencillo de usar - tenemos que llamar a un nombre de función (aquí es ``factorial``), dar la lista de argumentos requerida (sólo hay uno - ``n``), y capturar el valor de retorno con alguna variable.

.. code-block:: Python

  result = factorial(10)

También podemos ir un paso más allá, y en lugar de calcular la probabilidad como en el caso anterior, también podemos escribir una función que la calcule. Incluso podemos ponerle un nombre para que no quede ninguna duda. Una vez programada, podemos utilizar la función ``factorial`` tantas veces como queramos.

.. activecode:: en_main_0904
    :caption: 5 lanzamientos de moneda ganados en 10 intentos, versión con dos funciones
    :enabledownload:

    def factorial (n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    def chance_of_k_wins_in_n_coin_tosses(k, n):
        chance = (factorial(n) / (factorial(k) * factorial(n-k))) * (0.5 ** k) * (0.5 ** (n-k))
        return chance

    print ('La posibilidad de ganar cinco veces en 10 lanzamientos es {: .1f}%'. format (chance_of_k_wins_in_n_coin_tosses(5, 10) * 100))


.. topic:: Ejercicio

  Intenta programar una función que calcule la probabilidad de \\(k\\) ganar en \\(n\\) intentos para cualquier juego, no sólo *lanzamiento de moneda justa* donde cara y cruz son igual de frecuentes, sino para juegos donde no tiene por qué ser así (por ejemplo, al lanzar una moneda falsa). Tenemos que escribir una función que considere los distintos valores de la variable :math:`p \in [0, 1]` en la primera fórmula de esta lección, no sólo 0,5 (como para la moneda justa). En ese caso, la función debe tener una lista de 3 argumentos ``k, n, p``. Puedes utilizar el *ActiveCode* anterior o programar dicha función en tu entorno de desarrollo favorito.
