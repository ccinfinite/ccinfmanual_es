.. _strings:

*******
Strings
*******

.. index:: string, string, str, índice

Creamos una cadena de caracteres utilizando un par de apóstrofes ``"..."`` o comillas ``'...'`` entre los que puedes introducir cualquier carácter que encuentres en el teclado, e incluso los que no encontrarás en él.
En Python, un tipo que representa una cadena se llama ``str``.

Podemos ver la cadena ``'Spam and Eggs'`` como si fuera
un conjunto de caracteres, donde el primer carácter es ``S``, el segundo es ``p``, el tercero es ``a``, y así sucesivamente. Así que podemos numerar los caracteres de una cadena de este tipo. En Python, es habitual empezar a numerar desde ``0``. En este caso, el primer carácter de la cadena estará representado por el número ``0``, y el último por el número ``string_length  - 1``. Para nuestra cadena, la letra ``S`` corresponderá al número ``0``, la letra ``p`` al número ``1``, la letra ``e`` al número ``2``, etc. Estos números (0, 1, 2...) se llaman **índices**.
Los índices se utilizan para señalar un lugar específico en una cadena de caracteres. Al señalar un lugar concreto, por ejemplo, el quinto, nos referimos a un carácter con índice 5. El quinto índice será, en este caso, el sexto carácter de la cadena (en nuestro caso, la letra ``a``).
Esta referencia se hace con corchetes:

.. code-block:: python

    string[index]

Basándonos en la cadena ``'Spam and Eggs'``, nos referiremos a los índices ``2`` y ``5``, es decir, al tercer y sexto lugar de la cadena, respectivamente. Veamos cómo funciona en el *ActiveCode*.

.. activecode:: en_main_0301
    :caption: Indización de secuencias

    text = 'Spam and Eggs'
    print('(a) text[2] =', text[2])
    print('(a) text[5] =', text[5])


.. index:: len, longitud de la secuencia

El último índice de la variable ``text`` es el número 12 y es igual a
``string_length - 1``. Para calcular la longitud de la secuencia, podemos utilizar la función ``len('text')``, lo que significa que el último índice disponible de la secuencia ``seq`` viene dado por la fórmula ``len(seq) - 1`` y con esta expresión, podemos referirnos al último elemento de la secuencia.

.. code-block:: Python

     seq[len(seq) - 1]

El penúltimo elemento será ``seq[len (seq) - 2]`` etc.
Imprimir (y calcular) la longitud de la secuencia cada vez no es
particularmente ventajoso, y Python nos permite hacer referencia *desde el final de la secuencia* simplemente usando índices negativos.

.. activecode:: en_main_0302
    :caption: indexación de la secuencia con índices negativos

    text = 'Spam and eggs'
    print('(last s) text[12] ->', text[12])
    print('(last s) text[len(text) - 1] ->', text[len(text) - 1])
    print('(last s) text[-1] ->', text[-1])
    print('(last g) text[-2] ->', text[-2])
    print('(S) text[-13] ->', text [-13])


La siguiente tabla resume las posibilidades de referenciar lugares individuales en la secuencia ``s = "Python"``.

=============== ========== ========== ========== ========== ==========
P               y          t          h          o          n
=============== ========== ========== ========== ========== ==========
0               1          2          3          4          5
len(s) - len(s) len(s) - 5 len(s) - 4 len(s) - 3 len(s) - 2 len(s) - 1
-len(s)         -5         -4         -3         -2         -1
=============== ========== ========== ========== ========== ==========

Operaciones con cadenas
~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: concatenación

Podemos realizar varias operaciones con variables simples como los números. Podemos sumarlas, restarlas... ya hablamos de ello :ref:`in the previous chapter <variables>`. Con las cadenas no es diferente. Podemos sumar dichas cadenas utilizando el operador ``+``.

.. code-block:: python

     >>> 'Spam' + 'eggs'
     'Spameggs'

También podemos multiplicarlas por un número (aunque sólo del tipo ``int``).

.. code-block:: python

     >>> 'Spam' * 3
     'SpamSpamSpam'

Pruebe usted mismo

.. activecode:: en_main_0303
     :caption: operaciones de cadena simples

     print('Spam' + 'eggs')
     print('Spam' * 3)

Todo funciona igual cuando primero capturamos la cadena en una variable.

.. .. showeval:: showEval_concat_multi
..    :trace_mode: true
..
..    tekst = 'Mielonka'
..    inny_tekst = 'jajka'
..    ~~~~
..    {{'Mielonka' + 'jajka'}}{{tekst + inny_tekst}}
..    {{'Mielonka' * 3}}{{tekst * 3}}


.. activecode:: en_main_0304
     :caption: operaciones sencillas con variables de cadena

     text = 'Spam'
     other_text = 'eggs'
     print(text + other_text)
     print(text * 3)


Echa un vistazo al shortcode de abajo.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = (text[8] + text[4]) * 2


.. fillintheblank:: fitb-en_main_03ex1

   ¿Qué valor asignará a la variable "resultado"?

   - :gaga: Correcto.
     :ga: Véase la multiplicación por 2 al final.
     :e e e: En Python, la indexación comienza desde 0.
     :x: Por desgracia, no. Fíjate en los números entre corchetes: ¿a qué letras se refieren esos números? Junta esas letras y no olvides multiplicar por 2. ¿Quizás no hayas incluido los apóstrofes (o las comillas) en tu respuesta?


Podemos comparar las cadenas entre sí. Si queremos preguntar si dos cadenas son iguales (constan de los mismos caracteres), podemos preguntar.

.. activecode:: en_main_0305
      :caption: comparación de cadenas

      text = 'Spam'
      same_text = 'Spam'
      other_text = 'eggs'
      print('¿Es el Spam un Spam?', text == same_text)
      print('¿El Spam no eggs?', text != other_text)

Es un poco más complicado para otras operaciones de comparación. En este caso, Python compara las cadenas elemento por elemento. Para un par de caracteres, el intérprete compara los valores de los caracteres de la tabla de caracteres dada (ASCII, Unicode ...). Si los valores difieren en el siguiente par de caracteres, la cadena con el mayor valor en la tabla de caracteres se encuentra como la mayor. Puede aprender más sobre las comparaciones de cadenas en la sección sobre manipulación de cadenas.


.. _sequence_slicing:

Corte de secuencias
~~~~~~~~~~~~~~~~~~~

También podemos utilizar los índices para referirnos a una parte mayor de una secuencia. Podemos utilizarlos en el *corte de secuencias*. Un método general de corte de secuencias es el siguiente:

.. code-block:: python

    seq[start:stop:step]

Como puede ver, generalmente tenemos que dar tres números:

* ``start`` - el índice a partir del cual se inicia el corte
* ``stop`` - índice en el que termina el corte, se omite el valor correspondiente a este número
* ``step`` - el tamaño del incremento

Veamos un ejemplo sencillo. Tenga en cuenta que para todos los argumentos, podemos sustituir los números positivos y negativos y el cero.

.. activecode:: en_main_0306
    :caption: corte de secuencias

    text = 'Spam and eggs'
    print('(egg) de 9 a 12 cada 1:', text[9:12:1])
    print('(mne) de 3 a 12 cada 3:', text[3:12:3])
    print('(Sa n gs) de principio a fin cada 2', text[0:len(text):2])
    print('(sgge dna) del final al 4 con un paso negativo', text [-1:4:-1])

Un tipo particular de paso es el que es igual a ``1``. Si ``step = 1`` significa que nos referimos a cada índice posterior en el rango deseado. Es un valor por defecto del incremento, y podemos omitirlo.

.. code-block:: python

  >>> text[2:5] == text [2:5:1]
  True

Los índices específicos son ``0`` y ``-1`` (``len(seq) - 1``). Significan
el inicio y el final de la secuencia. Estos lugares son obvios para el intérprete, y podemos omitirlos.

.. activecode:: en_main_0307
    :caption: Indices 0 y -1

    text = 'Spam and eggs'
    print('de inicio a 5 cada 1:', text[:5])
    print('de 8 al final de cada 2:', text[8::2])
    print('una copia de la secuencia - de principio a fin cada 1', text[:])
    print('secuencia inversa', text[::-1])

Se pueden hacer referencias similares a cualquier tipo de secuencia.

Volvemos a la tarea anterior. Esta vez queremos utilizar la variable ``text`` para asignar ``'nana`` a la variable ``result``, pero esta vez utilizando la estructura de corte de cadenas anterior.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = text[start:stop:-1] * 2

.. fillintheblank:: fitb-pl_main_03ex2

    ¿Cómo será el código para que la variable de resultado contenga la cadena ``'nana'``?

    result = tekst[|blank|:|blank|:-1] * 2

    - :5: Poprawnie.
      :6: Język Python numeruje pozycje w łańcuchu od zera.
      :x: Niestety nie.
    - :3: Poprawnie.
      :4: Nie do końca. Pamiętaj, że druga wartość nie jest uwzględniana w wycinaniu. Ponadto język Python numeruje pozycje w łańcuchu od zera.
      :x: Niestety nie.


.. index:: operador in, operador not in, in, not in

Operadores de membresía
-------------------------

Existen operadores de pertenencia especiales para las secuencias. El operador ``in`` comprueba si un elemento está presente en la secuencia. El operador hermano ``not in`` comprueba si falta un elemento.

.. activecode:: en_main_0308
     :caption: Membership operators: in, not in

     text = 'Spam and eggs'
     print('¿Está S en el texto?', 'S' in text)
     print('¿Z no está en el texto?', 'z' not in text)

Como puedes ver, el resultado de estas operaciones es un valor lógico, ya sea ``True`` o ``False``.

.. _strings_concatenation:

.. index:: concatenación, función str

Concatenación de cadenas y la función ``str``
---------------------------------------------------

En uno de los ejemplos anteriores, hemos utilizado el operador ``+`` para sumar dos caracteres. Podemos concatenar cadenas de cualquier longitud y, como resultado, crear una nueva cadena más larga.

.. activecode:: en_main_0309
     :caption: Concatenation

     print('Spam' + 'and' + 'eggs')
     variable = 'Spam and eggs.' + ' ' + 'Eggs and spam.'
     print(variable)

Si quisiéramos generar una secuencia de caracteres de esta manera adjuntando el número (``int`` o ``float``) a una cadena, entonces simplemente sumando dichas cantidades, veremos que obtenemos un error.

.. index:: casting, función str

.. code-block:: python

    >>> number_of_cats = 2
    >>> 'Ala tiene ' + number_of_cats + ' cats.'
    TypeError: cannot concatenate 'str' and 'int' objects

En cierto modo, podríamos esperar esto porque el operador ``+`` denota algo diferente para los números y las cadenas. Para crear *dinámicamente* una cadena usando el operador ``+`` y la variable numérica ``number_of_cats`` y obtener ``'Ala tiene 2 gatos.'``, tendremos que convertir de alguna manera el número ``2`` al tipo ``str``.
Es lo que se conoce como **casting** del tipo ``int`` al tipo ``str``. Para realizarlo, utilizamos la función con el nombre del tipo solicitado. Aquí será ``str`` ya que necesitamos una representación en forma de cadena del número ``2``. Basta con *envolver* la variable que nos interesa con la función ``str(number_of_cats)``. Esta función devolverá la representación de una variable dada como tipo cadena.

.. activecode:: en_main_0310
    :caption: Concatenación con fundición

    number_of_cats = 2
    print ('Ala ma ' + str(number_of_cats) + ' cats.')

La mayoría de los tipos incorporados tienen esta representación, y podemos añadir cualquier otro literal a las cadenas. ¡A continuación se muestra un ejemplo con una lista, pero siéntase libre de probar diferentes tipos también!

.. activecode:: en_main_0311
    :caption: Una función que proyecta str

    fib_list = [1, 1, 2, 3, 5, 8, 13, 21]
    fib_info = '8 palabras de la secuencia de Fibonacci son ' + str(fib_list)
    print(fib_info)

El método ``format``
=====================
Las cadenas tienen un método incorporado llamado ``format``, que permite crear las cadenas de forma bastante dinámica sin necesidad de concatenación o de una función de fundición, que a veces puede ser complicada. El método ``format`` es una forma más fácil de hacer eso.

Si tenemos tres variables, ``a, b, c`` y queremos insertarlas en la cadena usando la función ``format``, escribimos:

.. code-block:: python

    '{}, {}, {}'.format(a, b, c)

También podemos utilizar números para especificar el orden de los argumentos. Permite que el orden de las variables en la cadena sea diferente del orden de los argumentos de la función.

.. code-block:: python

    '{0}, {1}, {2}'.format(a, b, c)
    '{2}, {1}, {0}'.format(a, b, c)

También se pueden utilizar variables varias veces.

.. code-block:: python

    {0}{1}{0} '.format('abra', 'cad')

También podemos utilizar nombres de argumentos.

.. code-block:: python

    'Coordinates: {lat}, {long}'.format(lat='37.24N ', long='- 115.81W')

Es una buena idea usar la función 'format' para crear cadenas, ya que te da mucho control sobre el resultado. Esta lección sólo toca el tema de las cadenas y su formato. Recomendamos el `documento PyFormat <https://pyformat.info>`_ o leer sobre la manipulación de cadenas en el manual hermano de este proyecto si quieres aprender más sobre el formateo de cadenas.

.. activecode:: en_main_0312
    :caption: str.format()

    a, b, c = 'one', 'two', 'three'
    print('{}, {}, {}'.format(a, b, c))
    print('{0}, {1}, {2}'.format(a, b, c))
    print('{2}, {1}, {0}'.format(a, b, c))
    print('{0}{1}{0}'.format('abra', 'cad'))
    print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N ', longitude='-115.81W'))
