.. _oop:

**********************************
Programación orientada a objetos
**********************************

.. index:: class, instance, object

Llegamos al último capítulo de este manual. Hablaremos de los objetos. En el caso del lenguaje Python, todo es un objeto, así que este debería ser probablemente el primer capítulo, pero es demasiado tarde para eso.
No discutiremos los objetos, las clases y sus instancias muy a fondo aquí. Sólo te diremos cómo construir rápidamente una **clase** y su **instancia**, que representa esta clase.

    Todo es un objeto porque puede ser asignado a una variable o ser un argumento de una función.

Para construir un objeto que represente algún dato y permita su manipulación (cambio, extracción de información relevante...), necesitamos programar la clase. En Python, utilizamos la siguiente construcción.

.. code-block:: Python

  class ClassName:
    BODY_CLASS

Las clases pueden representar cualquier objeto inventado por nosotros. Podemos construir clases que describan un pato, una pera o una mesa de café. Estos "objetos" reales tienen atributos físicos específicos, como el color y el peso, pero también podemos pensar en las características que distinguen a los objetos. Para el pato y la pera, puede ser su especie. Para una mesa, puede ser el tipo de material.

La clase define el estado y el comportamiento de los objetos. Podemos utilizarla para describir qué propiedades tiene un objeto determinado y qué podemos hacer con ellas.
Vamos a escribir una clase ``Pear`` para ilustrar la idea. Podemos describir dicha pera hablando de su variedad, color (que probablemente esté relacionado con la variedad), peso, tamaño (relacionado con el peso), sabor...

.. code-block:: Python

    class Pear:
        "'
        El objeto Pera describe las propiedades de las peras.
        "'

        def __init __ (self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

Ya está. Hemos construido una clase que describe las peras. Todavía no hay nada interesante que podamos hacer con esta clase. Hemos desarrollado una clase que define un **objeto**. Ahora podemos crear uno.
Puedes notar que justo debajo de la definición de ``clase Pera:`` ponemos la cadena ``''El objeto Pera describe las propiedades de las peras'''``. Al igual que con las funciones, es una cadena que representa la documentación de la clase (docstring). Escribir la documentación es siempre una gran idea.
Vamos a crear un objeto basado en esta clase.

.. activecode:: en_main_1201
    :caption: Pear

    class Pear:
        '''
        La clase Pera describe las propiedades de las peras.
        '''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

    pear = Pear('General Leclerc', 130, 'agrio y dulce')
    print(pear.variety)

Ahora la variable ``pera`` almacena un objeto de la clase ``Pera``.
Si trato la clase como un patrón que necesita ser llenado, entonces el objeto ``pera`` será sólo una copia de dicho patrón de la clase "Pera", pero *con valores específicos (argumentos)* dados a los parámetros formales de la clase. En este caso:

    ``variety`` <- 'General Leclerc'
    ``weight`` <- 130
    ``taste`` <- 'agrio y dulce'

.. index:: instance, instance attribute, class attribute

Lo llamaremos **instancia de clase** o simplemente **una instancia**. Esta instancia tiene sus **atributos**: ``variation``, ``weight`` y ``flavor``, que en este caso son idénticos a los **atributos de la clase**. Podemos referirnos a ellos como a las variables ordinarias (porque también son variables) utilizando la *notación de punto*.

.. code-block:: Python

    >>> pear.variety
    'General Leclerc'

Creando una nueva instancia de la clase con una llamada

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'agrio y dulce')

rellenamos los parámetros que declaramos en la definición de la función ``__init __(self, variant, weight, taste)``, una función excepcional, como verás en un minuto.

.. index:: init, __init__

Función ``__init__``
======================

Esta excepcional *función especial* ``__init__`` se utiliza para pasar argumentos a las nuevas instancias de la clase ``Pear``. Para utilizar los argumentos en cualquier parte de la clase (objeto) tenemos que asignarlos a **atributos**. Podemos dividir los atributos en atributos de clase y de instancia. Los que pertenecen a las instancias comienzan en la clase con el prefijo ``self``.

.. index:: self

La variable ``self`` es una representación de instancia. Cada función en el cuerpo de la clase debe comenzar con ``self`` [#self]_. En nuestro ejemplo,

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'agrio y dulce')

dentro del cuerpo de la clase, ``self`` se convierte mágicamente en ``pear``. De hecho, ``self`` no tiene que llamarse ``self``. Puede llamarse de cualquier manera. Se llame como se llame (aunque recomendamos ``self``), será una **variable que representa una instancia dentro del cuerpo de la clase**. Para referirse a cualquier atributo de instancia, debemos preceder este atributo en la clase con la variable ``self`` y utilizar el punto de referencia.

.. index:: get, set, getter, setter

Getters y setters
====================

No debemos referirnos directamente a los atributos de la instancia (``variety, weight, flavor``), independientemente de que queramos leer el valor que hay allí o asignar uno nuevo. Existen funciones especiales llamadas **getters** (para leer, sus nombres comienzan con la palabra ``get_``) y **setters** (para cambiar el valor, ``set_``). Se construyen exactamente igual que las funciones normales (con ``def``), y su primer argumento tiene que ser ``self``.

.. activecode:: en_main_1202
    :caption: Pear - getters y setters

    class Pear:
        '''La clase Pera describe las propiedades de las peras.'''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

        def get_weight(self):
            return self.weight

        def get_variety(self):
            return self.variety

        def get_taste(self):
            return self.taste

        def set_weight(self, x):
            self.weight = x

        def set_variety(self, x):
            self.variety = x

        def set_taste(self, x):
            self.taste = x


    # instancia
    pear = Pear('General Leclerc', 330, 'agrio y dulce')

    # referencia al método get_taste() de la instancia de la pera
    print("¿Cuál es el sabor de una pera?")
    print("La pera es", pear.get_taste())

    # establecer la variable de sabor con set_taste(TASTE) para la instancia de la pera
    pear.set_taste('extremadamente dulce')
    print("¿Cuál es el sabor de una pera?")
    print("La pera es", pear.get_taste())

Como puedes ver, las funciones ``get_taste`` o ``set_taste`` se llaman como funciones normales, pero hay un nombre de instancia delante (aquí: ``pear``). Ten en cuenta que aunque en el cuerpo de la clase ``Pear`` la función ``get_taste`` (línea 15) tiene un parámetro formal ``self``, al llamar a esta función (líneas 42 o 48) **no proporcionamos un argumento**. Esta es una notación típica (interfaz) para llamar a funciones que pertenecen a una instancia. A estas funciones las llamamos **métodos**.

    **Método** es una función relacionada con la instancia. Con los métodos podemos influir en el estado de los objetos (instancias).

En particular, toma un argumento menos que su homólogo (función) en el cuerpo de la clase porque ``self`` recibe la dirección de la instancia (``pear``) desde la que llamamos al método. El resto de los argumentos aparecen en la llamada como en una función tradicional.
De nuevo - mágicamente el nombre de la instancia es sustituido en la clase bajo la variable ``self``.

.. index:: str, __str__

La clase ``Square``
=====================

Ahora es el momento de un ejemplo de clase más significativo. Construiremos una clase relativamente simple para representar cuadrados.
La llamaremos ``Square``. ¡Qué sorpresa! Nombrar las clases con letras mayúsculas en la convención *CamelCase* es una buena práctica recomendada por los desarrolladores de Python. La clase tendrá una funcionalidad mínima: calculará el área y el perímetro de los cuadrados.

.. activecode:: en_main_1203
    :caption: Clase Square
    :enabledownload:

    class Square:
        "" "Clase Square" ""

        def __init__(self, a):
            self.set_side(a)

        def get_side(self):
            return self.side

        def set_side(self, var):
            if isinstance(var, (int, float)) and var > 0:
                self.side = var
            else:
                print('El lado debe ser un número > 0')

        def area(self):
            return self.get_side() ** 2

        def perimeter(self):
            return 4 * self.get_side()

        def __str__(self):
            return "{} de área {} y perímetro {}".format(
                self.__class__.__name__, self.area(), self.perimeter())


    k1 = Square(10)
    print(k1)
    k2 = Square(3.1415)
    print(k2)

Experimenta con la clase anterior. Construye otros cuadrados.
Cuando hayas terminado, lee el párrafo siguiente. Debería aclarar qué y cómo funciona dicha clase. Por último, te esperan algunos ejercicios.

Para describir un cuadrado, sólo necesitamos un parámetro: la longitud del lado. Conociendo este parámetro, podemos calcular cualquier cantidad que describa un cuadrado, como su área o su perímetro. El parámetro formal ``a`` lo proporcionamos al crear la instancia. Esta vez, para pasar el valor del parámetro ``a`` de la función ``__init__`` al atributo de la instancia llamado ``side`` (visible en el programa anterior como ``self.side``), utilizamos la función de ayuda ``self.set_side(a)``. Esta función está escrita de forma defensiva y no permitirá especificar valores que no sean números positivos del tipo ``int`` o ``float``. Sin embargo, si el usuario decide introducir algún otro valor, verá la información ``Side debe ser un número > 0``, y la variable ``side`` no se creará. Con la entrada correcta, como en los dos ejemplos de instancia, la variable ``side`` tomará el valor dado por el usuario, por lo que podremos calcular el área y el perímetro del cuadrado.

Ambas funciones de utilidad ``area`` y ``perimeter`` toman sólo un parámetro formal ``self``, es decir, la instancia correspondiente. Gracias a esto, el conjunto completo de atributos y métodos asociados a la instancia estará disponible para estas funciones a través de la construcción ``self.ATTRIBUTE``. Entre otras cosas, tenemos el método ``get_side`` que devuelve la longitud del lado de un cuadrado. Debemos referirnos a él para poder calcular el área y el perímetro. Podríamos utilizar la llamada a ``self.side`` directamente. Sólo será una forma diferente de calcular las mismas características sin utilizar la idea de getter y setter. Ambos enfoques tienen sus pros y sus contras. Las funciones en sí son simples. Al calcular el área, elevamos la longitud del lado a la segunda potencia, y al calcular el perímetro, sumamos las longitudes de los cuatro lados iguales.

El siguiente concepto nuevo es la función especial ``__str__``. ¿Todavía recuerdas: ref:`problem of concatenating strings <string_concatenation>`? Para convertir cualquier objeto en una cadena, teníamos que utilizar la función de proyección ``str``. Esta función busca en los objetos de la función especial ``__str__`` y la llama. Así que, al construir dicha función, creamos una representación de nuestro objeto en el tipo ``str``. Además, la función ``print`` también muestra lo que programamos en la función ``__str__``. Al crear una clase, vale la pena dedicar algo de tiempo a la programación de esta función especial. Gracias a ella, podemos describir claramente la instancia del objeto con la que posteriormente trabajaremos.


.. topic:: Ejercicio

    Añadir una función que calcula la longitud de la diagonal a la clase ``Square``. Utiliza la fórmula :math:`d = a \sqrt{2}`.

.. topic:: Ejercicio

    A partir de la clase ``Square``, construye la clase ``Rectangle``. Recuerda, que en general, tiene dos lados, ``a`` y ``b`` de diferentes longitudes.


.. rubric:: Footnotes

.. [#self] mentimos un poco aquí - no todas las funciones tienen que empezar con ``self``, pero por ahora, vamos a dejarlo así.
