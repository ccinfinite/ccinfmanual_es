.. _modules:

.. index:: module

**********
Modulps
**********

.. index:: descomposición, abstracción

Cuando creamos un programa de ordenador y resolvemos un problema con su ayuda, solemos construir varias funciones para resolver partes específicas del problema. En :ref:`a lesson on functions <subroutines>` escribimos dos funciones - una calculaba el factorial y la otra la probabilidad de un cierto número de victorias en unos lanzamientos de moneda. La primera (``factorial``) era una función auxiliar que permitía reducir el código, mejorar su legibilidad y limitar la posibilidad de cometer un error. En otras palabras, permitía una **descomposición** del problema principal en subproblemas (o más bien sus soluciones). Además, hacía que el uso futuro de la función factorial fuera independiente de su implementación. Podríamos mejorar este código de alguna manera (por ejemplo, empezar "range" con el número 2 en lugar del 1). Esto no afectaría a la solución del problema del lanzamiento de la moneda, que construimos con la función ``factorial``. Esta independencia de la implementación del uso se llama **abstracción**. La descomposición y la abstracción son las dos razones principales para utilizar funciones.

.. index:: importar

Hay más problemas de combinatoria que requieren el cálculo de factoriales. En definitiva, si vamos a trabajar en problemas de combinatoria, estaría bien tener esta función a mano y utilizarla sin tener que definir la función factorial cada vez. Una opción es utilizar dicha función programada en el módulo ``math``. Para usarla, necesitamos importarla.

.. code-block:: python

  from math import factorial

A partir de ahora, podemos utilizarla igual que la función que hemos escrito. Así, podemos calcular de cuántas formas diferentes podemos formar las letras "abc". Utilizaremos la fórmula para la permutación de un conjunto de 3 elementos - la respuesta es :math:`3!` (mira cualquier libro de texto de matemáticas)

.. activecode:: en_main_1101
   :caption: Permutaciones y factorial

   from math import factorial
   word = 'abc'
   ans = factorial(len(word))
   print('{}! = {}'.format(len(word), ans))

Técnicamente hablando, un módulo matemático es sólo un archivo que contiene definiciones e instrucciones de Python. El nombre del archivo es el nombre del módulo con la extensión ``.py``.

.. index:: import, from

Uso de módulos
=====================

Para utilizar las funciones de los módulos, tenemos que importarlos al espacio de nombres. Tenemos dos opciones. La primera se encuentra arriba.

.. code-block:: python

  from module import object
  # e.g
  from math import factorial

Ahora podemos referirnos a un ``objeto`` (una función) como si lo hubiéramos definido nosotros mismos en el programa en el que estamos trabajando, como en el ejemplo anterior. Si queremos, podemos cambiar el nombre del objeto importado por el que queramos utilizando la construcción ``from module import object as nice_name``.

.. activecode:: en_main_1102
   :caption: Importar una función de un módulo

   from math import factorial as fact
   word = 'abc'
   ans = fact(len(word))
   print('{}! = {}'.format(len(word), ans))


Si necesitamos varias funciones de un módulo, podemos importarlas como una lista.

.. code-block:: python

  from module import object1, object2
  # e.g
  from math import factorial, pow

Esto permitirá el uso de ambas funciones en el programa. En el ejemplo siguiente, hemos importado las funciones ``factorial`` como ``fact``, y la función ``pow`` con el mismo nombre.

.. activecode:: en_main_1103
   :caption: Importar dos funciones del módulo

   from math import factorial as fact, pow
   k, n = 5, 10
   print('La probabilidad {} de ganar en {} lanzamientos de moneda es igual a '.format(k, n), end = '')
   print('{:.1f}%'.format(100 * fact(n) * pow(0.5, 10) / fact(k)**2))


La segunda forma es importar el módulo completo y referirse a las funciones (y otros objetos) que contiene con una referencia *dot*. Aquí también podemos importar un módulo utilizando el nombre que más nos convenga.

.. code-block:: python

    import module
    import module as other_name

Volveremos a calcular de cuántas formas podemos ordenar las letras "abc", pero llamaremos a la función a través del módulo y de la referencia del punto.

.. activecode:: en_main_1104
   :caption: referencia de dot a una función

   import math
   word = 'abc'
   ans = math.factorial(len(word))
   print('{}! = {}'.format(len(word), ans))


Nuestro módulo
=================

Si quisiéramos construir nuestro módulo conteniendo, por ejemplo, varias funciones útiles en combinatoria, basta con crear un fichero con un nombre significativo e incluir estas funciones en este fichero. En nuestro caso, llamaremos al fichero, y por tanto al módulo ``combinatorics.py``. Añadiremos todas las funciones que hemos programado en las lecciones anteriores.

.. literalinclude:: combinatorics.py
    :linenos:
    :emphasize-lines: 1, 17
    :caption: Descargar :download:`combinatorics module <combinatorics.py>`.

Para utilizar la biblioteca recién creada, es necesario importarla utilizando cualquiera de los métodos descritos anteriormente.

.. literalinclude:: es_main_ch11e01.py
    :linenos:
    :emphasize-lines: 1
    :caption: Descarga :download:`este script <es_main_ch11e01.py>`.
