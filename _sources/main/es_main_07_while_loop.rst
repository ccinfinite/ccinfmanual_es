.. _while_loop:

***************
Bucle ``While``
***************

A veces tenemos que ejecutar una secuencia de instrucciones una y otra vez, incluso sin fin. En los lenguajes de programación, utilizamos bucles en estos casos. Python tiene dos declaraciones de este tipo que funcionan con principios ligeramente diferentes.

.. index:: while, loop

La construcción universal de Python que permite que un bloque de sentencias se ejecute varias veces es la sentencia while. Al igual que la ramificación, se basa en una condición lógica.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK

Mientras la condición se cumpla (sea igual a ``True``), se ejecutará el bloque de instrucciones ``WHILE_BLOCK``. Como puedes ver, el bloque de instrucciones comienza de nuevo después de los dos puntos. Es hora de un ejemplo sencillo de una operación de bucle. Para devolver los números del 1 al 5 elevados a la segunda potencia, podemos utilizar el concepto de bucle.

.. activecode:: en_main_0701
    :caption: cuadrados de números del 1 al 5

    number = 1
    while number <= 5:
        square_number = number ** 2
        print ('{}^2 = {}'. format (number, square_number))
        number = number + 1

La última línea (número 5) es esencial. Si nos olvidamos de ella, lo que ocurre inesperadamente a menudo, el bucle continuará eternamente.
A continuación, encontrarás una tabla que presenta todos los valores de la variable ``number `` y el cuadrado y el resultado de la prueba ``number >= 5`` paso a paso. Ten en cuenta que la variable ``square_number`` no se calculará para ``número = 6``.

+------+--------------+-----------+
|number|square_number |number <= 5|
+======+==============+===========+
|  1   |      1       |    True   |
+------+--------------+-----------+
|  2   |      4       |    True   |
+------+--------------+-----------+
|  3   |      9       |    True   |
+------+--------------+-----------+
|  4   |     16       |    True   |
+------+--------------+-----------+
|  5   |     25       |    True   |
+------+--------------+-----------+
|  6   |              |   False   |
+------+--------------+-----------+

Podemos resolver cualquier tarea de programación con dos construcciones condicionales, ``if`` y ``while``. No siempre será fácil, a veces puede parecer imposible, pero normalmente podemos hacerlo. Ahora intentaremos encontrar números divisibles por 2 y 3, pero mayores que 33 y menores que 67.

.. activecode:: en_main_0702
    :caption: números divisibles por A y B en el límite de inicio a fin

    start, stop = 33, 67
    A, B = 2, 3

    num = start
    while num <= stop:
        if (num % A == 0) and (num % B == 0):
            print('{} es divisible por {} and {}'.format(num, A, B))
        num = num + 1

Como puede ver arriba, hemos puesto ambas condiciones entre paréntesis en la declaración condicional. De esta manera, podemos *agrupar* cálculos o instrucciones que deben ejecutarse por separado, y no estamos seguros de que una notación sin paréntesis funcione. En este caso, eliminar los paréntesis no cambiará nada, ya que el producto lógico ``and`` tiene una de las `prioridades más bajas entre los operadores <https://docs.python.org/3/reference/expressions.html#operator-precedence>`_. Independientemente de la precedencia de la operación, los paréntesis a menudo aumentan significativamente la legibilidad del código, y le animamos a utilizarlos (¡con prudencia!).

.. topic:: Ejercicio

    Modifica el programa anterior para calcular la **suma** de todos los números divisibles por 2 y 3, mayores que 33 y menores que 67.

.. index:: while - else

En cuanto a las ramas, podemos utilizar la sentencia ``else`` en el bucle ``while``.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK
    else:
        ELSE_BLOCK

Podemos entenderlo precisamente como la construcción ``if-else``. Cuando no se cumpla la ``CONDITION``, se ejecutará el bloque de sentencia bajo 'else'. Tenemos dos opciones de este tipo:

* el bucle ``while`` se ejecutará hasta el final, y el bloque ``else`` será llamado después de él

    .. code-block:: python

        i = 1
        while i < 8:
            print(2 ** i)
            i += 1
        else:
            print('fin de bucle')

* el bucle ``while`` no se ejecutará en absoluto (la condición no se cumplirá),
   el bloque ``else`` se ejecutará

    .. code-block:: python

        i = 10
        while i < 8:
            print(2 ** i)
           i += 1
        else:
            print('el bucle while no se ejecutará en absoluto')


.. index:: break, continue

Las frases "break" y "continue
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si esperamos que el bucle se rompa en algún momento, podemos utilizar la palabra clave ``break``.

.. activecode:: en_main_0703
    :caption: break

    j = 10
    while True:
        if j > 23:
            print('para: j = {} es mayor que 23'.format(j))
            break
        j += 3

.. note:: **while True**

    Esta construcción se utiliza cuando queremos construir un bucle sin fin. Como la condición ``True`` siempre será verdadera, tenemos que utilizar otra forma de detener el bucle. Podemos utilizar la sentencia ``break`` como en el ejemplo anterior. También podemos llamar a :ref:`subroutine <function>`, que controlará el programa. Pronto hablaremos de las subrutinas.


La sentencia ``continue`` tiene precisamente el comportamiento contrario. Cuando se encuentra, las instrucciones del bloque del bucle dejarán de ejecutarse, y el bucle volverá a girar, independientemente del estado del programa.

.. activecode:: en_main_0704
    :caption: continue (the number -3 will not print)

    j = -5
    while j <= 5:
        j += 1
        if j == -3:
            print('omitimos {}'.format(j))
            continue
        print('cubo {} a {}'.format(j, j**3))


.. topic:: Ejercicio

    A veces, cuando ejecutamos un programa informático, éste nos hace varias preguntas, normalmente necesarias para configurarlo correctamente. A veces la respuesta se limita a decidir "sí" o "no", por lo que si escribimos "ok", el programa no sabrá lo que queremos decir - tendremos que introducir precisamente la palabra "if" o la palabra "no", y si damos otra, entonces el programa volverá a la misma pregunta y seguirá preguntando hasta que introduzcamos una de estas palabras. Utilizando un bucle infinito y la sentencia ``break``, escribe un programa de este tipo. A continuación presentamos el esqueleto de este programa. En lugar de los comentarios, introduce tu código.

.. activecode:: en_main_0705
    :caption: ejercicio
    :enabledownload:

    while True:
        ans = input('Introduzca sí o no: ')
        # si es sí: imprime '¡Acuerdo!' y termina el bucle (programa)
        # si no: imprime '¡No consentido!' y termina el bucle (programa)
        # si es diferente: pregunta de nuevo para introducir el sí o el no
        break
