def factorial(n):
     '''
     La función calcula el factorial.

     IN:
       n (INT) - número natural

     OUT:
       INT: factorial de n! = 1 * 2 * 3 * ... * n
     '''
     f = 1
     for number in range(2, n + 1):
       f = f * number
     return f


def chance_of_k_wins_in_n_coin_tosses (k, n):
     '''
     Una función que calcula la probabilidad de k aciertos en n intentos de lanzar una moneda al aire

     chance = n! / (k! * (n-k)!) * 0.5^n

     IN:
       k (INT) - número de aciertos
       n (INT) - número de intentos

     OUT:
       FLOAT: probabilidad de k aciertos en n intentos
     '''
     chance = factorial(n) / factorial(k) / factorial(n-k) * 0.5**n
     return chance
