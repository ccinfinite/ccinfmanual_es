********
Prefacio
********

En 1977, Ken Olsen, fundador y director durante mucho tiempo de Digital Equipment Corporation, declaró *"No hay ninguna razón para que alguien quiera tener un ordenador en su casa "*. Curiosamente, él mismo era el propietario de un ordenador de este tipo. Si hablamos de los ordenadores, era por supuesto la prehistoria. Hoy en día, nadie se pregunta por la utilidad de los ordenadores. A veces ayudan a curar a la gente, otras veces se utilizan simplemente para mantenerse en contacto con los amigos. Los ordenadores personales se encuentran en más del 48% de los hogares del mundo. Lo utiliza una media del 15% de las personas en el mundo. Algo más del 35% de nosotros utiliza teléfonos inteligentes, también un tipo de ordenador. Si añadimos que utilizamos Internet una media de más de 6 horas al día, resulta que la profecía del señor Olsen fue una de las opiniones más fallidas de la historia. En total, permanecemos en dos terrenos irreales durante más de la mitad de nuestra vida, ya que dormimos una media de casi 7 horas al día.

La mayoría de nosotros utilizamos el ordenador, el smartphone o la tableta. Todos usamos el navegador para navegar por Internet o aplicaciones dedicadas para ver o editar fotos. Utilizamos programas para escribir cartas, redacciones, correos electrónicos y para contactar a través de las redes sociales. Algunos trabajamos con aplicaciones más especializadas, otros sólo necesitan un navegador web y un cliente de correo electrónico.

A veces nos encontramos con una situación en la que el programa que utilizamos actualmente nos permite realizar todas las tareas necesarias, otras veces nos falta alguna funcionalidad y tenemos que resolver la tarea de otra manera. A veces se puede encontrar un programa diferente que sirva, otras veces no. A veces, la realización de las tareas programadas será extremadamente lenta, pero es posible llevarla a cabo utilizando los programas informáticos conocidos. Entonces sólo necesitamos tiempo y paciencia (y, preferiblemente, alguien que compruebe si todo está bien).

Aprender el lenguaje de programación no consiste en dominar uno de los lenguajes de programación. Hay miles de ellos - desde los más `populares <https://www.tiobe.com/tiobe-index>` como Java, C, Python, C ++, C #, PHP, hasta los extremadamente esotéricos como el ya histórico INTERCAL o el minimalista Brainf*ck.

Esto es obviamente un paso necesario, pero no es el objetivo principal.
La habilidad más importante que se adquiere es la capacidad de entender el corazón del problema al que nos enfrentamos y dividirlo correctamente en sus primeras partes, como podremos resolver fácilmente y luego poner estas soluciones juntas para obtener la respuesta a la pregunta original. Así es como trabaja un programador típico. Este método tiene incluso su nombre - divide y vencerás y es uno de los principales paradigmas de programación y algoritmos.

El aprendizaje de este modelo puede ser el mayor valor añadido de este curso. Puedes intentar encontrar una salida a diferentes situaciones de la vida simplemente resolviendo partes más pequeñas del gran problema al que te enfrentas. A veces también vale la pena programar tu próximo día de manera escalonada.

.. note::
    Puedes probar los siguientes programas de inmediato en un cuaderno Jupyter (IPython).
    Por favor, descarga este archivo, súbelo a tu servidor Jupyter o ábrelo
    en Google Colaboratory.

    file: `notebook <https://nbviewer.org/urls/bitbucket.org/ccinfinite/ccinfmanual_es/raw/4655441f92fd56698d1495b1d11b463afda00299/_sources/main/es_main_01_intro_code.ipynb>`_

¿Cómo organizar las fotografías?
=================================

Es hora de poner algún ejemplo, alguna aplicación más específica de toda esta idea de programación. Muchas personas tienen un archivo de fotos digitales en sus ordenadores o en discos duros externos. A menudo guardamos estas fotos en catálogos, a los que damos un nombre con un lugar que hemos visitado o con el evento al que hemos asistido. A veces añadimos una fecha al nombre, y otras veces tenemos catálogos de padres asociados a la fecha, por ejemplo

::

    -2019
      ᒻ--Viaje a Cracovia
      ᒻ--Espectáculo de fuegos artificiales
      ᒻ--Fiesta de cumpleaños de John

    -2018
      ᒻ--Vacaciones en Bari
      ᒻ--Fin de semana en París

Las fotos que están dentro, suelen tener el nombre dado por la cámara, que suele ser así

::

  IMG20180821232.JPG  IMG20180821233.JPG  IMG20180821234.JPG  IMG20180821235.JPG
  IMG20180821236.JPG  IMG20180821237.JPG  IMG20180821238.JPG  IMG20180821239.JPG
  IMG20180821240.JPG  IMG20180821241.JPG  IMG20180821242.JPG  IMG20180821243.JPG
  IMG20180821244.JPG  IMG20180821245.JPG  IMG20180821246.JPG  IMG20180821247.JPG

Quizá, sólo por pasión por el orden, nos gustaría que las fotos de los catálogos tuvieran también nombres significativos que identificaran inmediatamente el contenido de la foto, por ejemplo `2018_Holidays_in_Bari_001.jpg`. El método más sencillo y pesado será simplemente renombrar manualmente todos estos archivos. Otro podría ser el uso de uno de los programas de gráficos que pueden hacer tal cambio en el llamado modo por lotes. También puede escribir un simple programa de Python ...

.. literalinclude:: main_intro_example1.py
   :linenos:
   :caption: Enlace a :download:`el script <main_intro_example1.py>`.

Lo único que hay que saber es que en lugar de "2019"
tenemos que introducir el nombre del catálogo, o
incluso el año que nos interesa, y en el lugar
"John birthday party" introducimos el nombre del
directorio en el que almacenamos las fotos cuyos nombres
sólo queremos cambiar. Por supuesto - también necesitamos
saber cómo ejecutar dicho programa, pero no tenemos que
preocuparnos por el cambio de nombre en sí o por el hecho
de que nos equivoquemos al numerar las fotos. Una vez que
dominemos la capacidad de ejecutar programas de Python
que escribimos nosotros mismos, utilizaremos los mismos
comandos cada vez.

Parece un poco complicado, pero pronto lo aprenderemos todo. Lo importante es que sólo necesitamos 12 líneas de código (aunque en realidad son 10) para que el ordenador haga todo el trabajo por nosotros. Además, este programa se puede modificar de forma muy sencilla para que busque en la carpeta principal en la que guardamos las fotos y cambie automáticamente los nombres de todos los archivos de forma similar -dando al principio del nombre de la foto un año, luego el nombre del evento y finalmente su propia numeración-.


Bien, es hora de un problema más académico...

¿Con qué frecuencia aparece una palabra en un texto escrito?
=============================================================

Seguramente todos los que leen libros se preguntaron alguna vez qué palabra aparece
más a menudo. O, ¿existe alguna regla que describa la relación
con la frecuencia de la palabra y el rango de la palabra...
Vale, probablemente nadie está pensando realmente en esas cosas,
más bien todo el mundo se centra en el contenido del libro ;)
Sin embargo, es un problema bastante interesante (al menos para nosotros).

La idea es contar cuántas veces aparece una palabra en un texto
texto considerado. Si consideramos un texto sencillo, por ejemplo, la canción "Hello, Goodbye" de los Beatles, podemos contar simplemente cuántas veces aparece una palabra determinada en él.

    You say, „Yes”, I say, „No”

    You say, „Stop” but I say, „Go, go, go”

    Oh no

    You say, „Goodbye”, and I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello”

No tengamos en cuenta el caso de las letras, siguen siendo las mismas palabras. Recomendamos a todo el mundo que cuente el número de veces que aparece una determinada palabra (son 15) y que luego ordene las palabras de la más común a la más rara. Debería obtener una tabla similar

========== ==========
say        10
i          7
hello      7
you        5
go         3
goodbye    3
no         2
don’t      2
know       2
why        2
yes        1
stop       1
but        1
oh         1
and        1
========== ==========

¿Es correcto? Si el texto es sencillo, la tarea no es difícil. Probemos con un texto un poco más complicado.

    Having conceived the idea he proceeded to carry it out with
    considerable finesse. An ordinary schemer would have been content
    to work with a savage hound. The use of artificial means to make
    the creature diabolical was a flash of genius upon his part. The
    dog he bought in London from Ross and Mangles, the dealers in
    Fulham Road. It was the strongest and most savage in their
    possession. He brought it down by the North Devon line and walked
    a great distance over the moor so as to get it home without
    exciting any remarks. He had already on his insect hunts learned
    to penetrate the Grimpen Mire, and so had found a safe
    hiding-place for the creature. Here he kennelled it and waited
    his chance.

    But it was some time coming. The old gentleman could not be
    decoyed outside of his grounds at night. Several times Stapleton
    lurked about with his hound, but without avail. It was during
    these fruitless quests that he, or rather his ally, was seen by
    peasants, and that the legend of the demon dog received a new
    confirmation. He had hoped that his wife might lure Sir Charles
    to his ruin, but here she proved unexpectedly independent. She
    would not endeavour to entangle the old gentleman in a
    sentimental attachment which might deliver him over to his enemy.
    Threats and even, I am sorry to say, blows refused to move her.
    She would have nothing to do with it, and for a time Stapleton
    was at a deadlock.

No es tan fácil. En primer lugar, se trata de muchas más palabras. En segundo lugar, no suelen repetirse en absoluto. No presentaremos aquí toda la tabla, ya que hay hasta 154 filas, pero las palabras más comunes tienen este aspecto

========== ==========
the        14
to         11
his        9
it         8
a          8
and        8
he         7
was        6
with       4
of         4
in         4
would      3
========== ==========

El ordenador es la herramienta perfecta para realizar las mismas tareas una y otra vez.
Los informáticos dicen que para hacer lo mismo muchas veces, hay que decirle al ordenador que utilice *bucles*.
Ya has visto un ejemplo de su uso en el problema anterior.

En el siguiente programa, la idea de usar un bucle se da tres veces. En la primera, comprobamos letra por letra si el carácter del texto original aparece en el alfabeto inglés. En la segunda contamos las palabras una a una. El tercer bucle `for` se utiliza para mostrar los resultados, desde la palabra más común hasta la más rara.

El programa en sí mismo es interactivo, puede iniciarlo haciendo clic en el botón "Ejecutar", también puede cambiarlo, romperlo y repararlo.

.. activecode:: Zipf_law
   :coach:
   :caption: Un programa interactivo que cuenta el número de veces que aparece una determinada palabra en el texto.

   txt = """
   You say, „Yes”, I say, „No”
   You say, „Stop” but I say, „Go, go, go”
   Oh no
   You say, „Goodbye”, and I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello”
   """

   fixed_txt = ""
   for letter in txt.replace("\n", " "):
       l = letter.lower()
       if l in "abcdefghijklmnopqrstuvwxyz ’":
           fixed_txt += l

   zipf = {}
   for word in fixed_txt.split():
       if word in zipf:
           zipf[word] += 1
       else:
           zipf[word] = 1

   for z in sorted(zipf.items(), key=lambda x: x[1], reverse=True):
       print("{0[0]}\t{0[1]}".format(z))

Echa un vistazo a las líneas 11, 17 y 23. Puedes ver las palabras clave ``for``. Esto significa que acabamos de programar tres bucles. El bucle es un mecanismo que permite a la computadora realizar algunas tareas una y otra vez.

Ahora, intenta sustituir el texto por otro.


¿Pero cómo funcionaría para todo el libro? El anterior fragmento de prosa procede de la novela policíaca de Sir Arthur Conan Doyle, titulada El sabueso de los Baskerville. En la traducción que encontrará en la página web del Proyecto Gutenberg `gutenberg.org <https://www.gutenberg.org/ebooks/2852>` _ el texto completo tiene más de 62.000 palabras y nos plantea una tarea bastante imposible. Es decir, por supuesto que se pueden contar las palabras dadas utilizando un método sistemático, pero, este tipo de tareas secuenciales de "encontrar y añadir" debería hacerlas la máquina.

Entonces, ¿cómo se maneja todo el libro?

.. literalinclude:: main_intro_example2.py
   :linenos:
   :emphasize-lines: 4,10,20
   :caption: Link para :download:`este script <main_intro_example2.py>`.

Copia este código en el cuaderno Jupyter, o descarga y ejecuta el programa en la consola. Debería calcular la cardinalidad y mostrarte las 10 primeras palabras más comunes del libro.


Ahora comprueba tus conocimientos.

.. mchoice:: question1_intro
    :multiple_answers:
    :correct: b,c
    :answer_a: Todas las palabras son igualmente probables.
    :answer_b: La palabra en la posición (n) aparece (1/n) veces más que la más frecuente.
    :answer_c: La probabilidad de una observación es inversamente proporcional a su rango.
    :answer_d: Deberíamos leer más libros.
    :feedback_a: La verdad es que no. Piensa en lo popular que es la palabra "el", y lo impopular que es la palabra "Brexit".
    :feedback_b: Cierto. Esta es la afirmación más común para la ley de Zipf.
    :feedback_c: Correcto. Este sería el enunciado más general para la ley de Zipf.
    :feedback_d: Aunque definitivamente debemos leer mucho, Zipf nunca afirmó eso.

    ¿Qué significa la ley de Zipf?


.. fillintheblank:: intro_fill_01

    ¿Qué es el comando Python para el bucle?

    - :for|while: Exactly!
      :.*: En realidad no... consulte el texto anterior.


.. index:: comment

Comentarios
=============

El carácter ``#`` precede a los comentarios en python.
Todos los caracteres que siguen a ``#`` y que se extienden hasta el final de la línea son
ignorados [#comment]_.

.. code-block:: python

   >>> # la primera línea del comentario
   >>> print("Hello World!") # segunda línea del comentario

La documentación de funciones o clases formada por texto encerrado entre comillas triples o apóstrofes puede considerarse un tipo de comentario. No es un comentario *per se*, pero puede serlo. Puede leer más sobre la documentación más adelante en este manual.

.. code-block:: python

   """
   este texto va precedido de,
   y termina con tres comillas,
   es, por tanto, un carácter literal ordinario,
   que se utiliza a menudo para
   crear documentación de programas
   """

Un lenguaje de programación
=============================

Un programa informático creado mediante un lenguaje de programación es el método más común para hacer que un ordenador realice el trabajo que hemos planificado. Si este trabajo no consiste en responder a los correos electrónicos, sino en realizar algunas actividades recurrentes que son menos estándar, entonces escribir tu propio programa será probablemente el método más sencillo. Puede sonar extraño al principio del aprendizaje de la programación, pero esperamos que cambies de opinión al final de este curso. Todos podemos entender el lenguaje de programación como un método de comunicación con un ordenador que es comprensible para un humano. Podemos compararlo con un lenguaje natural que permite la comunicación entre personas. Sin embargo, hay una diferencia. En el caso de los lenguajes de programación, no hay lugar para la subestimación o la interpretación. Hay que enunciar todos los mensajes con mucha precisión. El ordenador es una máquina y no puede (todavía) elaborar la expresión ambigua real.




.. rubric:: Footnotes

.. [#comment] puede dar lugar a veces a problemas entre el estándar de codificación del texto utilizado en el script, y el declarado en la cabecera del mismo. Lo más seguro es utilizar el sistema UTF-8.
