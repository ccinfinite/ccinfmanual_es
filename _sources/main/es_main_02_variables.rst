.. _variables:

*********
Variables
*********

.. index:: literal, operador, primitivos

Empezaremos discutiendo las construcciones más simples de las que están hechos prácticamente todos los lenguajes de programación.
Podemos desglosar estas construcciones *primitivas* en **literales**, que son valores que el programador utiliza y **operadores** para manipular estos literales (valores).

Quizá el ejemplo más sencillo sea la suma de dos números. Todos conocemos el resultado de la suma: math:`2 + 2`.
Para ver el resultado de la suma en el *ActiveCode* de abajo, necesitamos envolver la acción con la función ``print``.
Prueba el código de abajo, y luego sustituye los números y la acción de sumar (``+``) por otra cosa que recuerdes de tu lección de matemáticas.

.. activecode:: en_main_0201
    :coach:
    :caption: Operaciones con números

    print(2 + 2)

.. index:: variable, operador de asignación

También podemos recordar el resultado de la acción. Para ello, creamos **variables** en los lenguajes de programación. Una variable se hace de forma muy sencilla - tenemos que introducir su nombre, luego poner el signo igual (**operador de asignación**) y el valor que queremos asignar a esta variable, por ejemplo:

.. code:: Python

   variable = 2

En la ventana de abajo, puedes probar a crear variables. En este caso, asignamos inmediatamente el resultado de la suma de los dos números ``2 + 2`` a la variable ``addition_result``, y la diferencia ``2 - 2`` a la variable ``subtraction_result``.

.. activecode:: en_main_0202
   :coach:
   :caption: Crear una variable

   addition_result = 2 + 2
   print ('Resultado de la adición:', addition_result)
   subtraction_result = 2 - 2
   print ('Resultado de la sustracción:', subtraction_result)


.. topic:: Exercise

    En la celda anterior, añade nuevas líneas del programa, en las que calcularás e imprimirás (usando ``print``) el producto y el cociente de dos números cualesquiera. Puedes seguir utilizando ``2``, pero quizás quieras probar algo más emocionante...

Como en los ejercicios de matemáticas, los cálculos apropiados pueden agruparse utilizando paréntesis. Lo hacemos por varias razones: para evitar un error o para especificar la secuencia de acciones. Si queremos sumar el valor doble de los dos números 3 y 4, escribiremos ``2 * 3 + 2 * 4`` o

.. activecode:: en_main_0203
    :coach:
    :caption: Paréntesis

     result = 2 * (3 + 4)
     print (result)


.. index:: data type

En general, los paréntesis se utilizan para agrupar operaciones sobre cualquier variable de cualquier *tipo*, y no se limita a las operaciones con números. Los paréntesis permiten controlar el orden de ejecución de las sentencias. En la mayoría de los lenguajes de programación, podemos utilizar valores (literales) con diferentes propiedades. El valor que proporcionemos y la representación visual estarán definidos por el **tipo** de los datos.

.. topic:: Ejercicio

    Mientras compraba, encontró dos pares de zapatos que quiere comprar. Los rojos costaban 40 euros. Los azules costaban 56 euros pero con un 25% de descuento. Por compras superiores a 85 euros, ¡obtendrás un 20% de descuento adicional!

    ¿Qué par de zapatos costará más? Calculemos el precio con descuento de los zapatos azules, y luego puedes marcar la respuesta adecuada a continuación.

    .. activecode:: en_main_0204
       :coach:
       :caption: Paréntesis - Ejercicio

       blue_price = 56
       discount = (100 - 25) / 100
       new_price = blue_price * discount
       print (new_price)

    .. clickablearea:: en_main_ex0201
      :question: ¿Qué par de zapatos cuesta más?
      :table:
      :correct: 1,2
      :incorrect: 1,1
      :feedback: Ejecuta el *ActiveCode* anterior y comprueba el resultado

      +------+-------+
      | red  | blue  |
      +------+-------+

    .. fillintheblank:: en_main_ex0202
       :casei:

       ¿Cuánto costarán los dos pares?

       - :82: Excelente!
         :x: De forma incorrecta. Intenta calcular la suma en el *ActiveCode* anterior.

    .. clickablearea:: en_main_ex0203
          :question: Si decide comprar los dos pares, ¿podrá obtener un descuento adicional?
          :table:
          :correct: 1,2
          :incorrect: 1,1
          :feedback: Vuelve al *ActiveCode* y suma ambos números

          +------+------+
          | yes  | no   |
          +------+------+


Tipos de datos básicos
======================

.. index:: tipo int, tipo float

Tipos numéricos
~~~~~~~~~~~~~~~~

Hemos visto varios tipos de datos en los ejemplos anteriores. Literales como ``2`` o ``2.0`` representan el número 2 (un literal numérico con el valor 2) pero de dos tipos diferentes. El literal ``2`` tiene el valor 2 y se le asigna el tipo entero (``int``). El segundo, ``2.0``, también tiene el valor 2, pero es de tipo punto flotante (``float``). Podemos tratar ambos simplemente como el número 2, pero la forma en que se almacenan en la memoria del ordenador será diferente. Puedes leer más sobre los números en la otra parte de este proyecto.

.. index:: bool type, True, False

Tipo booleano
~~~~~~~~~~~~~~

Los tipos de datos particulares difieren en los valores de conjunto permitidos y en las operaciones permitidas que podemos realizar con ellos.
Uno de los tipos de datos más utilizados es el tipo lógico (``bool``).
Puede tomar los valores ``True`` para la verdad lógica o ``False`` para el falso lógico. Por ejemplo, sabemos que el número 2 es mayor que el número 1. Por ello, escribiendo: math:`2> 1`, sabemos que la expresión es verdadera. Podemos programar una operación de este tipo.

.. code:: Python

    >>> 2 > 1
    True

Además, podemos asignar el resultado de dicha comparación (``True``) a una variable.

.. activecode:: en_main_0205
    :coach:
    :caption: Tipo booleano

    print(2 > 1)
    truth = 2 > 1
    print(truth)

Si ahora volvemos al problema de la compra de zapatos, en lugar de juzgar qué par es más caro mirando las facturas, podemos dejar las conclusiones al intérprete.

.. activecode:: en_main_0206
    :coach:
    :caption: Problemas con el calzado

    red_shoes = 40
    blue_shoes = 56 * 0.75
    print (red_shoes > blue_shoes)


.. index:: operador de comparación

Los operadores básicos están asociados al tipo lógico, con los que podemos comparar variables y obtener verdadero o falso como resultado de dicha comparación. Tenemos el operador ``>`` (mayor que), que ya conocemos, pero también el operador ``<`` (menor que). También podemos comprobar si una variable es "mayor o igual que" (``>=``) y "menor o igual que" (``<=``) otra variable. Comprobamos la igualdad utilizando el doble signo de igualdad ``==``, que es probablemente el origen de uno de los errores más comunes que cometen los estudiantes de primer año. Comprobamos las desigualdades utilizando ``! =``. En el siguiente *CódigoActivo*, puedes probar los operadores de comparación según tus ideas.

.. activecode:: en_main_0207
    :coach:
    :caption: Comparar operaciones

    print(100 > 10)

Podemos realizar las operaciones de comparación en todos los objetos. Todos tienen la misma prioridad, y el intérprete los analizará de izquierda a derecha.
En Python, también podemos comparar si dos objetos son iguales (más sobre objetos en el capítulo sobre :ref:`object-oriented programming <oop>`). Los operadores ``is`` y ``is not`` nos dicen precisamente eso. Sin embargo, no serán de mucha utilidad en este curso.

.. index:: tipo NoneType, None

Tipo 'NoneType'
~~~~~~~~~~~~~~~~

También existe un tipo de datos particular en Python llamado
``NoneType``. Dentro de este tipo, sólo encontramos un valor,
``None``. Se utiliza para expresar la falta de un valor
significativo, la no existencia de datos, o el almacenamiento
de un contenido *vacío*. También es el valor devuelto por la
función cuando omitimos la palabra clave ``return`` o cuando
no damos ningún valor después de esta palabra. Puedes aprender
más sobre las funciones en los capítulos de :ref:`later <subroutines>`
de este manual.

.. index:: función de tipo

Función ``type``
~~~~~~~~~~~~~~~~~

Para comprobar de qué tipo es un objeto dado, puedes utilizar la función ``type(obj)``.

.. code-block:: Python

     >>> type(3)
     <type 'int'>
     >>> type(7.5)
     <type 'float'>
     >>> type('a')
     <type 'str'>
