.. _branching:

********************
Branching
********************

.. index:: bool, booleano, verdadero, falso

Ya hemos hablado del tipo de datos lógico ``bool``. Dentro de este tipo, sólo podemos distinguir dos valores: ``True`` o ``False``. Muchos lenguajes de programación toman el número ``0`` para falso y ``1`` (o cualquier otro número en general) para verdad.
Existen operadores lógicos estándar definidos para este tipo.

.. index:: casting, casting explícito

El nombre del tipo lógico es también el nombre de la función ``bool(obj)``, que permite cambiar (cast) la variable ``obj`` a un tipo lógico.

.. activecode:: en_main_0601
   :caption: bool, boolean type

   print("bool(1):", bool(1))
   print("bool(-3):", bool(-3))
   print("bool(0):", bool(0))
   print("bool(None):", bool(None))
   print("bool([1, 2, 3]):", bool([1, 2, 3]))
   print("bool([]):", bool([]))
   print("bool('Andy has a cat'):", bool('Andy has a cat'))
   print("bool(''):", bool(''))


Sabemos por lecciones anteriores que los operadores lógicos básicos son ``and`` (lógico and), ``o`` (lógico or) y ``no`` (lógico negación). También encontrarás un resumen de los operadores de comparación (``<``, ``==``, etc.). Las operaciones con ellos también dan lugar a literales del tipo ``bool``.
Este tipo es la base para la construcción de las ramas del programa.

**Branching** es un lugar en un programa de ordenador donde decidimos lo que el programa debe hacer a continuación, basado en una estructura lógica. En el caso más simple, el programa se divide en dos partes.

La sentencia ``if``.
====================

.. index:: if, branch

En Python, la sentencia de bifurcación del programa es ``if``. Consideremos un problema de este tipo.

  Si el valor de la variable ``x`` es negativo, cámbialo a un número positivo, de lo contrario no hagas nada.

Podemos *traducir* esta tarea matemática a un lenguaje de programación.

  Si la variable ``x`` es menor que 0, cambia su signo por el contrario, en caso contrario no hagas nada.

Como puedes ver, tenemos dos posibilidades - o el número ``x`` es negativo o no lo es. Se trata de una bifurcación. Para escribir un programa que haga tal tarea, necesitamos usar la sentencia ``if``.

.. code-block:: Python

    if x <0:
        x = -x

.. topic:: Ejercicio

    Esta no es la única forma de resolver el problema de cambiar números negativos a positivos. ¿De qué otra forma puedes cambiar números de negativos a positivos? Intenta encontrar otras dos formas y escribe un programa en Python.

..
  x *= -1
  x -= 2 * x
  x = abs(x)
  x = (x ** 2) ** 0.5

.. index:: indentación, bloque de código

.. note:: **Indentación**

    Para indicar que una parte del código pertenece a una rama determinada de la sentencia ``if``, debemos hacer una sangría en Python, es decir, desplazar una parte del código unos espacios a la derecha. Es habitual utilizar exactamente cuatro espacios para distinguir un determinado **bloque de código**. Cuando se aprende a programar, puede parecer redundante y engorroso. Afortunadamente, la mayoría de los procesadores de texto lo hacen automáticamente por nosotros. Basta con empezar un determinado bloque de código con el **colon** ``:``, para que el editor desplace automáticamente la siguiente línea de código en cuatro espacios. Si quieres, puedes utilizar cualquier número de espacios (e incluso tabuladores) para sangrar un bloque de código, pero los mejores programadores de Python utilizan cuatro.


El esquema completo de bifurcación de Python tiene este aspecto:

.. code-block:: python

    if CONDICION:
        BLOQUE_DE_INSTRUCCIÓN_SI
    elif CONDITION_1:
        BLOQUE_1
    elif CONDITION_2:
        BLOQUE_2
        ...
    else:
        BLOQUE_ELSE

Vemos dos nuevas sentencias: ``elif`` (que podemos leer como *else if*) y ``else`` (*otherwise*). La construcción obligatoria es utilizar la palabra clave ``if`` y el resto de palabras son opcionales. Sirven para una ramificación un poco más complicada. Python comprobará las condiciones anteriores una por una. Si encuentra una **condición** que es verdadera, ya no comprobará el resto de ellas. Es más, ni siquiera interpretará los bloques restantes. El intérprete sólo validará la sintaxis.

Veamos un ejemplo. Comprobaremos si la variable ``number`` apunta a un número par.

.. activecode:: en_main_0602
   :caption: números pares

   number = 12
   if number % 2 == 0:
       e_or_ne = 'un número par'
   else:
       e_or_ne = 'un número no par'
   print(e_or_ne)

Por supuesto, en el ejemplo anterior, veremos el mensaje ``un número par``. Juega con el código anterior y comprueba para qué números obtenemos números pares y para cuáles no. La paridad de un número significa que es divisible por dos sin resto, lo que significa que el cálculo del resto de la división utilizando el operador ``%`` debe devolver cero. Estamos comprobando este hecho en la primera rama ``if number% 2 == 0:``. Podemos responder "sí" o "no" a la pregunta sobre la paridad. No hay otra opción. En ese caso, si el número resulta no ser par (la condición discutida sería igual a ``False``), podemos utilizar la opción "en cualquier otro caso" porque es el único caso distinto. Por lo tanto, utilizamos la construcción ``if-else``. Nos enfrentamos a este tipo de bifurcación binaria muchas veces.

Para mostrar una bifurcación un poco más complicada, escribiremos un programa que nos informe de cuántos días tiene un mes determinado. Para distinguir tres posibles respuestas (31, 30 y 28/29), debemos utilizar la construcción ``if-elif-else``. Además, podemos protegernos contra una respuesta errónea si el usuario de nuestro programa proporciona un número de mes fuera del rango $[1, 12]$. En este caso, necesitaremos dos sentencias ``elif``.

.. activecode:: en_main_0603
   :caption: Número de días del mes.

   days_31 = [1, 3, 5, 7, 8, 10, 12]
   days_30 = [4, 6, 9, 11]
   month = int(input('Introduzca el número del mes (de 1 a 12): '))
   if month == 2:
       print('El mes numero {} tiene 28 o 29 días.'.format(month))
   elif month in days_31:
       print('El mes numero {} tiene 31 días.'.format(month))
   elif month in days_30:
       print('El mes numero {} tiene 30 días.'.format(month))
   else:
       print('Un año tiene 12 meses.')


.. topic:: Ejercicio

    Normalmente, cuando preguntamos por un mes, nos referimos a su nombre, no al orden del año. Intenta reescribir el programa anterior para que pida al usuario el nombre del mes y, en base a ello, devuelva información sobre el número de días. Probablemente será mejor programar la solución en algún otro entorno, no aquí en *ActiveCode*. Para hacerlo más fácil descarga :download:`this script <es_main_ch06e03.py>`.
