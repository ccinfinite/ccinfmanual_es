=====================
cc-infinite
=====================

**Computing Competences.
Innovative learning approach for non-IT students.**

.. figure:: img/ccinflogo.png
  :scale: 50

CC Infinite está dirigido a cualquier persona interesada en aprender a programar. No queremos limitar esta habilidad solo a estudiantes de TI o entusiastas de TI. Creemos que todas las personas, no necesariamente relacionadas con la industria de TI, pueden hacer que su trabajo con una computadora sea más agradable y acelerar su trabajo conociendo las reglas de programación. El objetivo principal del proyecto será aumentar la innovación y la interdisciplinariedad de la educación superior mediante el desarrollo de un curso de programación para estudiantes que no sean de TI y la evaluación de su eficacia en 4 universidades europeas.

Este manual fue creado como resultado del proyecto `CC Infinite <https://ccinfinite.smcebi.edu.pl>`_ cofinanciado bajo el programa de la Unión Europea `Erasmus+ <https://www.erasmusplus.eu/>`_.

#################################
Curso interactivo de programación
#################################

.. toctree::
   :maxdepth: 1

   main/es_main_01_intro.rst
   main/es_main_02_variables.rst
   main/es_main_03_strings.rst
   main/es_main_04_io.rst
   main/es_main_05_collections.rst
   main/es_main_06_conditions.rst
   main/es_main_07_while_loop.rst
   main/es_main_08_for_loop.rst
   main/es_main_09_functions.rst
   main/es_main_10_functions.rst
   main/es_main_11_module.rst
   main/es_main_12_oop.rst

Los materiales didácticos que ofrecemos han sido desarrollados como parte de un consorcio internacional.

.. figure:: img/uni.png
    :scale: 80

.. figure:: img/ccdisclaimer.png
    :scale: 80
